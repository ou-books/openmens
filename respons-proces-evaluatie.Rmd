---
output:
  pdf_document: default
  html_document: default
---
# Responsproces-evaluatie {#respons-proces-evaluatie}


```{r respons-proces-evaluatie-colofon, eval=FALSE, echo=FALSE, results="asis"}

md4e::knitWithClass(
  
  "Auteur: Gjalt-Jorn Peters en Natascha de Hoog; laatste update: 2022-08-25",

  class="colofon"
)

```

```{r respons-proces-evaluatie-summary, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="In dit hoofdstuk wordt besproken:",
  
  "responseproces-evaluatie.",

  class="overview"
)

```

```{r respons-proces-evaluatie-courses, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="Deze stof wordt behandeld in de volgende Open Universiteitscursus(sen):",
  
  "Onderzoekspracticum cross-sectioneel onderzoek (PB0812)",

  class="courses"
)

```

```{r respons-proces-evaluatie-dependencies, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="Dit hoofdstuk bouwt voort op deze andere hoofdstukken:",
  
  "Constructen",
  "Validiteit van meetinstrumenten",

  class="dependencies"
)

```

## Inleiding

Zoals uitgelegd in de hoofdstukken [Constructen](https://openmens.nl/constructen) en [Validiteit van meetinstrumenten](https://openmens.nl/validiteit-van-meetinstrumenten) gaat validiteit over of een meetinstrument meet wat het moet meten. Bij onderzoek bij mensen betekent dat meestal dat de scores op het meetinstrument worden veroorzaakt door het construct dat het meetinstrument moet meten. De vraag bij validiteit is dus of het meetinstrument 'goed werkt'. Net zoals een kapotte thermometer of weegschaal niet gebruikt kan worden in onderzoek naar temperatuur of gewicht, kan een meetinstrument dat het doelconstruct niet meet niet worden gebruikt in onderzoek bij mensen.

Een meetinstrument bestaat uit een procedure, eventuele stimuli en een of meerdere items. Een item bestaat uit een procedure, eventuele stimuli en een responsregistratie. Het toepassen van een meetinstrument bestaat eruit dat een deelnemer volgens de procedure wordt blootgesteld aan de stimuli en dat er een of meerdere responsen worden geregistreerd. Als het meetinstrument valide is, worden de geregistreerde responsen veroorzaakt door het doelconstruct (dat is, het construct dat het meetinstrument beoogt te meten).

Hoe het doelconstruct de respons veroorzaakt, wordt beschreven in het responsmodel van het meetinstrument. Responsmodellen starten vaak bij de perceptie en interpretatie van de procedure, stimuli en responsregistratie door de deelnemer, waarna het doelconstruct medebepaald hoe de respons op basis van die interpretatie tot stand komt.

Een eerste voorwaarde voor validiteit is daarom dat de procedure, stimuli en responsregistratie door de deelnemer worden geïnterpreteerd zoals bedoeld. Er zijn twee methoden om dit te onderzoeken: cognitieve interviews en responsproces-evaluatie. Cognitieve interviews worden verder besproken in het hoofdstuk [Cognitieve interviews](https://openmens.nl/cognitieve-interviews). In dit hoofdstuk bespreken we de responsproces-evaluatie.


## Responsprocessen

Het responsproces is het proces dat plaatsvindt wanneer iemand aan een item wordt blootgesteld. Het responsproces start met de perceptie van dat item (dat is, de responsregistratie). Dit betekent dat via een of meer van de zintuigen (zie het hoofdstuk [Stimuli](https://openmens.nl/stimuli)) informatie binnenkomt. Deze wordt vervolgens verwerkt en uiteindelijk resulteert dat in een respons. Dit wordt al snel abstract, dus laten we een concreet voorbeeld nemen.

Het drinken van alcohol is zeer schadelijk. Een enkel glas is in principe al ongezond, maar de schade neemt toe naarmate iemand meer alcohol consumeert. Via preventiecampagnes wordt het overmatig alcoholgebruik ontmoedigd. Voor de ontwikkeling van deze campagnes is het nodig om inzicht te hebben in waarom sommige mensen veel drinken en anderen niet [@peters_practical_2014]. Een belangrijk aspect hierbij is de intentie van mensen om hun alcoholgebruik te modereren. Dit kan onderzocht worden met de volgende vraag.

```{r respons-proces-evaluatie-alcohol-moderatie-intentie, results="asis", echo=FALSE}

if (knitr::is_latex_output()) {
  knitr::asis_output("
\\begin{table}[ht]
\\centering
\\begin{tabular}{p{5cm} p{15mm} p{.3cm} p{.3cm} p{.3cm} p{.3cm} p{.3cm} p{1.5cm}}
Ik ben van plan om volgende week maximaal 14 alcoholische drankjes te drinken. & absoluut niet & O & O & O & O & O & absoluut wel
\\end{tabular}
\\end{table}
");
} else {
  knitr::asis_output("
<table><tr><td>
Ik ben van plan om volgende week maximaal 14 alcoholische drankjes te drinken.
</td><td>absoluut niet</td>
<td>\U1F53E</td><td>\U1F53E</td><td>\U1F53E</td><td>\U1F53E</td><td>\U1F53E</td>
<td>absoluut wel</td>
</tr></table>
");
}

```

Een zeer eenvoudig responsmodel bij dit item kan uit de volgende stappen bestaan.

1. De deelnemer neemt het item waar, interpreteert de beelden en verwerkt de teksten.
2. De deelnemer begrijpt dat er een antwoord moet worden gegeven door een van de vijf opties te selecteren.
3. De deelnemer begrijpt dat het antwoord op een schaal van 'absoluut niet' tot 'absoluut wel' moet worden gegeven.
4. De deelnemer begrijpt dat er wordt gevraagd naar de eigen intentie om volgende week maximaal 14 alcoholische drankjes te drinken.
5. De deelnemer denkt na over diens plannen voor de volgende week.
6. De deelnemer schat in op welke dagen die van plan is om alcohol te drinken.
7. De deelnemer bedenkt of die van plan is om op deze dagen diens alcoholconsumptie te beperken zodat het gebruik over de gehele week de 14 drankjes niet overstijgt.
8. De deelnemer bepaalt welke van de vijf antwoordopties het beste correspondeert met de mate waarin de deelnemer van plan is diens alcoholconsumptie te modereren.
9. De deelnemer selecteert deze antwoordoptie.

De nummering is hier gebruikt om deelprocessen te onderscheiden. In dit responsmodel is een aantal deelprocessen sequentieel (stap 1 moet bijvoorbeeld plaatsvinden voor de overige stappen), maar andere deelprocessen kunnen parallel worden uitgevoerd (zoals het verwerken van de antwoordschaal en het verwerken van de vraagtekst). Als alle deelnemers dit item op deze manier verwerken (en het responsmodel is correct), dan is het item valide volgens de definitie van validiteit uit hoofdstuk [Validiteit van meetinstrumenten](validiteit-van-meetinstrumenten).

Het responsproces als reactie op dit item zou er vervolgens als volgt uit kunnen zien.

1. De deelnemer neemt het item waar, interpreteert de beelden en verwerkt de teksten.
2. De deelnemer begrijpt dat er een antwoord moet worden gegeven door een van de vijf opties te selecteren.
3. De deelnemer begrijpt dat het antwoord op een schaal van 'absoluut niet' tot 'absoluut wel' moet worden gegeven.
4. De deelnemer begrijpt dat er wordt gevraagd naar de eigen intentie om volgende week ongeveer 14 alcoholische drankjes te drinken.
5. De deelnemer denkt na over diens plannen voor de volgende week.
6. De deelnemer schat in op welke dagen die van plan is om alcohol te drinken.
7. De deelnemer bedenkt of die van plan is om op deze dagen ongeveer zoveel te drinken zodat over de gehele week ongeveer 14 drankjes worden gedronken.
8. De deelnemer bepaalt welke van de vijf antwoordopties het beste correspondeert met de mate waarin de deelnemer van plan is om ongeveer 14 drankjes te drinken.
9. De deelnemer selecteert deze antwoordoptie.

Dit responsproces wijkt op een fatale manier af van het responsmodel. In deelproces 4 begint het fout te gaan. Het is duidelijk dat dit item voor deze deelnemer niet valide is. De respons van de deelnemer is namelijk niet veroorzaakt door het doelconstruct.


## Responsproces-evaluatie

Het doel van responsproces-evaluatie (RPE) is om het responsproces van items in een meetinstrument te evalueren. Het doel hiervan is om te bepalen of het responsproces consistent is met het responsmodel van het meetinstrument. Bij een RPE biedt je de betreffende items aan deelnemers aan met daarbij per item een aantal metavragen die zijn ontworpen om het responsproces van dat item in kaart te brengen. Voorbeelden van algemeen bruikbare metavragen zijn 'Wat denk je dat deze vraag betekent?' en 'Waarom heb je het antwoord gegeven dat je gaf?'.

Een metavraag die bij het voorbeelditem hierboven gesteld kan worden, is bijvoorbeeld 'Wat betekent 'maximaal 14 alcoholische drankjes drinken' voor jou?'. Wolf et al. [-@wolf_survey_2019] geven in hun uitgewerkte voorbeeldvraag nog meer voorbeelden van metavragen. De items en de metavragen worden aan een klein aantal deelnemers gepresenteerd. Ongeveer vijf deelnemers lijkt te volstaan.

Metavragen zijn vaak, maar niet noodzakelijk, open vragen. Met RPE worden dus kwalitatieve data verzameld. De verkregen kwalitatieve antwoorden worden daarna gecodeerd door twee of meer experts wat betreft het doelconstruct en het meetinstrument. Deze codering bestaat bijvoorbeeld uit de categorieën begrepen/niet begrepen/onvoldoende informatie of consistent met responsmodel/inconsistent met responsmodel/onvoldoende informatie.

Als een item niet in alle gevallen is gecodeerd met de wenselijke categorie ('begrepen' of 'consistent met responsmodel'), moet het item worden aangepast en opnieuw worden aangeboden. Wanneer het item wel door alle experts voor alle deelnemers in de juiste categorie gecodeerd worden, wordt het item vervolgens aan een andere groep deelnemers gepresenteerd. Dit proces herhaalt zich totdat elk item voldoende vaak achter elkaar is gepresenteerd en telkens met de wenselijke categorie is gecodeerd. Dit lijkt ongeveer twintig opeenvolgende toedieningen te vereisen [@wolf_survey_2019].

Een korte presentatie van Melissa Wolf, een van de ontwikkelaars van RPE, waarin ze RPE uitlegt, is [hier](https://gauchocast.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=aa82e1ef-c6b3-4bcc-b265-ac6c0052d7bd) te bekijken. Houd er rekening mee dat de definitie van validiteit die ze geeft op de slides over 'What is Validity?' anders is dan de definitie die in dit boek wordt gehanteerd. 

<!-- NDH: Aangezien er nog geen hoofdstuk is over Cognitieve interviews is dit verschil ook (nog niet) relevant, dus tijdelijk uitgezet. -->

<!-- ## Responsproces-evaluatie versus cognitieve interviews -->

<!-- Responsproces-evaluatie en cognitieve interviews hebben elk een aantal unieke voor- en nadelen ten opzichte van elkaar. In de tabel hieronder staan deze op een rijtje. -->

<!-- ```{r respons-proces-evaluatie-rpe-vs-cis} -->

<!-- tab <- -->
<!--   matrix( -->
<!--       c(### Anonimiteit -->
<!--         "Als er alleen aantekeningen worden gemaakt, worden er geen persoonsgegevens verzameld. Als er audio-opnames worden gemaakt, wellicht wel (overleg met de privacy officer).", -->
<!--         "Als deelnemers goed worden geïnstrueerd om geen persoonsgegevens in de tekstvelden te typen (en die instructies ook opvolgen), worden er geen persoonsgegevens verzameld.", -->
<!--         ### Tijd -->
<!--         "Cognitieve interviews kunnen tijdrovend zijn. Er zijn vaak minstens twee rondes nodig en er moeten interviews worden ingepland en gehouden. Als het nodig is om audio-opnames te maken, moeten deze meestal worden getranscribeerd.", -->
<!--         "Responsproces-evaluatie kan relatief snel. Hoewel er veel iteraties nodig zijn, zijn in elke iteratie maar enkele deelnemers nodig. Bovendien zijn de antwoorden van deelnemers vaak kort en kunnen ze snel worden gecodeerd.", -->
<!--         ### Flexibiliteit -->
<!--         "Cognitieve interviews zijn uitermate flexibel. Als een deelnemer interessante informatie geeft over diens responsproces, kan de interviewer daarop doorvragen.", -->
<!--         "Omdat bij responsproces-evaluatie de metavragen van te voren worden opgesteld, is de flexibiliteit lager. Tussen de iteraties door kunnen metavragen wel worden aangepast." -->
<!--       ), -->
<!--       ncol=2, -->
<!--       byrow=TRUE -->
<!--     ); -->
<!-- row.names(tab) <- -->
<!--   c("Anonimiteit", "Tijd", "Flexibiliteit"); -->
<!-- colnames(tab) <- -->
<!--   c("Cognitieve interviews", -->
<!--     "Responsproces-evaluatie") -->

<!-- md4e::knitTable( -->
<!--   tab, -->
<!--   caption = "Een vergelijking van cognitieve interviews en responsproces-evaluatie", -->
<!--   longtable = TRUE, -->
<!--   cols = c("4cm", "4cm") -->
<!-- ); -->

<!-- # kableExtra::kable_styling( -->
<!-- #   kableExtra::column_spec( -->
<!-- #     kableExtra::column_spec( -->
<!-- #       kableExtra::kbl( -->
<!-- #         tab, -->
<!-- #         caption = "Een vergelijking van cognitieve interviews en responsproces-evaluatie", -->
<!-- #         booktabs = TRUE, -->
<!-- #         longtable = TRUE -->
<!-- #       ), -->
<!-- #       column = 1, width = "2cm" -->
<!-- #     ), -->
<!-- #     column = 2:3, width = "5cm" -->
<!-- #   ) -->
<!-- # ); -->

<!-- ``` -->
