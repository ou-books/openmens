
# Kruistabellen {#kruistabellen}


```{r kruistabellen-colofon, eval = TRUE, echo=FALSE, results="asis"}

md4e::knitWithClass(
  
  "Auteur: Peter Verboon; laatste update: 2021-05-04",

  class="colofon"
)

```

```{r kruistabellen-summary, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="In dit hoofdstuk wordt besproken:",
  
  "verband tussen categorische variabelen",
  "associatiematen bij een kruistabel",
  "chi-kwadraat toets",
  "inleiding correspondentieanalyse.",

  class="overview"
)

```



```{r kruistabellen-courses, eval=FALSE, echo=FALSE, results="asis"}

md4e::knitListWithClass(

  prefix="Deze stof wordt behandeld in de volgende Open Universiteitscursus(sen):",

  "Onderzoekspracticum inleiding data analyse",

  class="courses"
)

```



## Inleiding
 
In een kruistabel worden de frequenties van twee variabelen gezamelijk getoond. Deze variabelen zijn categorisch, met een nominaal of ordinaal meetniveau. De tabel toont de frequentie verdeling van de ene variabele voor elke categorie van de andere variabele en vice versa.
Om de samenhang tussen de twee categorische variabelen te quantificeren zijn er diverse associatiematen ontwikkeld. Ook kan er getoetst worden of het verband tussen de variabelen statistisch significant is.
In dit hoofdstuk worden eerst enkele associatiematen besproken, daarna wordt de chi-kwadraattoets besproken en hoe de samenhang tussen de variabelen kan worden gevisualiseerd.

```{r kruistabellen-data1, echo=FALSE, warning = FALSE, message=FALSE  }
 
  M <- matrix(c(4,5,5,3,5,6,7,4,8,10,9,6), nrow = 3, ncol = 4, byrow = T)
  row.names(M) <- c("Jong", "Middelbaar", "Oud")
  colnames(M) <- c("Benauwdheid", "Hoofdpijn","Smaakverlies","Overig")

 
 n <- sum(M)  
 rijSom <- rm  <- rowSums(M)
 cm <- colSums(M)
 kolSom <- c(cm, n)
 rp <- rm/n
 cp <- cm/n
 P <- M/n
 E <- rp %o% cp
 chisqr1 <- round(sum((n*P - n*E)^2 /(n*E)),2)
 
 rijProportie <- as.character(round(c(rm/n, 1),2))
 kolProportie <- as.character(c(round((colSums(M)/n), 2), 1, ""))
 
 M1 <- cbind(M,rijSom)
 M2 <- rbind(M1,kolSom)
 M3 <- cbind(M2, rijProportie)
 M4 <- rbind(M3, kolProportie)
 M5 <- round(diag(1/rowSums(M)) %*% M, 2)
 row.names(M5) <- row.names(M)
  
```

In tabel \@ref(tab:kruistabellen-tab1) wordt een verzonnen voorbeeld gegeven van mensen met een positieve corona test. de klachten zijn in vier categorieen ingedeeld: de 4 typen coronaklachten zijn gekruisd met drie leeftijdsklassen en in een kruistabel gezet. Het gaat hier dus om een nominale (`klachten`) en een ordinale (`leeftijdsklasse`) variable. Het aantal personen in deze tabel is $n=$ `r n`.

```{r kruistabellen-tab1}
 
     md4e::knitTable(M,
                  caption = "Kruistabel leeftijdsklasse versus klachten.")
 
                  
```

De vraag hier is of het patroon van klachten (klachtenprofiel of in het algemeen het rijprofiel) anders is voor de verschillende leeftijdsgroepen. Of anders geformuleerd of het leeftijdsprofiel anders is voor de verschillende klachten. Hiervoor voegen we allereerst de rijtotalen en rijproporties toe, evenals de kolomtotalen en kolomproporties.

```{r kruistabellen-tab2  }

   md4e::knitTable(M4, 
                  caption = 'Kruistabel leeftijdsklasse versus klachten met proporties.')
 
                  
```

We kunnen nu ook per leeftijdsklasse het klachtenprofiel berekenen door simpelweg de aantallen te delen door het desbetreffende rijtotaal. Dit is gedaan in tabel \@ref(tab:kruistabellen-tab3).
Wanneer deze profielen ongeveer gelijk zijn en dus ook ongeveer gelijk aan het totale profiel (dat zijn de kolomproporties zoals getoond in tabel \@ref(tab:kruistabellen-tab2), dan zijn de rijen en kolommen onafhankelijk. Er is dan geen verband tussen beide variabelen. In dit voorbeeld zijn de profielen ongeveer gelijk aan elkaar. Er is dus geen verband tussen `klachten` en `leeftijdsklasse`. Op dezelfde manier hadden we de leeftijdsprofielen per klacht kunnen bekijken, en daar zou noodzakelijkerwijs dezelfde conclusie uitkomen.

```{r kruistabellen-tab3  }
 
  md4e::knitTable(M5,   
                  caption = 'Profielen van de klachten per leeftijdsklasse.')
 
```


## Associatiematen

```{r kruistabellen-data2, echo=FALSE, warning = FALSE, message=FALSE  }
 

M <- matrix(c(3,4,5,1,2,  10,3,1,8,3, 1,4,6,12,6, 2,4,10,6,2, 4,4,8,2,4, 2,2,2,6,3), nrow = 5, ncol = 6)  
  row.names(M) <- c("Twitter", "Facebook", "Krant","Televisie","Online")
  colnames(M) <- c("GL", "PVV","VVD","CDA","D66","PvdA")
 
  nr <- dim(M)[1]
  nk <- dim(M)[2]
  df <- (nr-1)*(nk-1)
  
 km <- rowSums(M)
 rm <- colSums(M)
 n <- sum(M)  
 P <- M/n
  arp <- rm/n
  acp <- km/n
  rp <-  t(M)/rm
  cp <- (M)/km
 E <- (km/n) %o% (rm/n)
 R <- P - E
 I <- R/E
 Z <- I * sqrt(E)

 chisqr <- round(sum((n*P - n*E)^2 /(n*E)), 2)
 a <- ufs::cramersV(M)
 cramerV <- round(a$output$cramersV, 3)
 a <- ufs::confIntV(M)
 confIntV.fisher <- round(a$output$confIntV.fisher, 3)

                  
```

De $\chi^2$ is de belangrijkste maat om samenhang in een kruistabel aan te geven. Bij deze maat worden de geobserveerde waarden in de tabel vergeleken met de verwachte waarden, waarbij verwachte waarden, de waarden zijn onder de aanname dat er geen samenhang is tussen de rijen en kolommen. Een verwachte waarde in een cel wordt volledig bepaald door zijn rij- en kolomproportie. Als we de rijporporties voorstellen door $r$ en de kolomproporties door $c$ dan wordt de verwachte waarde ($e$) van cel $ij$ ($i^{de}$ rij en $j^{de}$ kolom):

\begin{equation}
e_{ij} = n \times r_i \times c_j
  (\#eq:kruistabellen-expected)
\end{equation}

De formule voor de $\chi^2$ ziet er als volgt uit:

\begin{equation}
\chi^2 = \sum{\frac{(n_{ij} - e_{ij})^2} {e_{ij}}}
  (\#eq:kruistabellen-chi2)
\end{equation}

Hierbij is $n_{ij}$ het geobserveerde aantal in cel $ij$, $e_{ij} = nr_{i}c_{j}$, het product van de rij- en kolomproporties maal het totaal aantal. Het somteken geeft hier aan dat over alle cellen wordt gesommeerd. 
De $\chi^2$ is afhankelijk van het aantal observaties en de grootte van de tabel.  
In het voorgaande voorbeeld was de $\chi^2$ = $`r chisqr1`$. Een hele lage waarde die aangeeft dat er geen samenhang in deze tabel is.


In tabel \@ref(tab:kruistabellen-tab4) staat een ander voorbeeld van een kruistabel. Het gaat hier om de politieke voorkeur en het favoriete nieuwsmedium. Aan $n =$ `r n` mensen is gevraagd op welke politiek partij zij nu zouden stemmen en welke medium zij vooral gebruikten om het dagelijks nieuws te volgen. Iedere cel in de kruistabel geeft het aantal mensen aan dat op de desbetreffende partij zou stemmen en het desbetreffende medium vooral gebruikt. De vraag is of er samenhang is tussen de nominale variabelen `politieke voorkeur` en het favoriete `nieuwsmedium`. Gebruiken mensen met een bepaalde politieke voorkeur, een ander medium dan mensen met een andere politieke voorkeur. Anders geformuleerd: zijn de profielen van nieuwsmediagebruik anders voor de verschillende politieke partijen.


```{r kruistabellen-tab4, echo=FALSE, warning = FALSE, message=FALSE  }
 

   md4e::knitTable(M, 
                  caption = 'Kruistabel Politieke voorkeur versus favoriete nieuws medium.')
 

```

Voor deze kruistabel geldt: $\chi^2 =$ `r chisqr`. Om te weten of dit een hoge of lage waarde is, moeten we ook de grootte van de tabel erbij betrekken. Bij het toetsen van de samenhang komen we hier op terug.

De tweede associatiemaat is $Cramer's\ V$. Dit is een maat die de samenhang aangeeft tussen twee nominale variabelen. De samenhang wordt uitgedrukt in ene getal dat tussen $0$ en $1$ ligt, waarbij $0$ aangeeft dat er geen enkele samenhang is, dus de rij- en kolomvariabele zijn volledig onafhankelijk van elkaar. Als de waarde $1$ is dan kan je de ene variabele perfect voorspellen uit de andere; er is volledige samenhang. Voor deze tabel geldt: $Cramer's\ V =$ `r cramerV`. $Cramer's\ V$ is gebaseerd op de $\chi^2$ en wordt als volgt berekend:

\begin{equation}
Cramer's\ V = \sqrt{\frac{\chi^2} {n(k-1)}}
   (\#eq:kruistabellen-cramersV)
\end{equation}

Hierbij is $k$ het minimum van het aantal rijen ($nr$) en kolommen ($nk$) in de tabel. Dus bij $5$ rijen en $4$ kolommen is $k = 4$ en $n$ is het totaal aantal observaties in de tabel. $Cramer's\ V$ is minder afhankelijk van de steekproefgrootte. Het is ook mogelijk om een betrouwbaarheidinterval (BI) rond de puntschatting van $Cramer's\ V$ te genereren. Het $95\%$ BI is hier: [`r confIntV.fisher`].


### Chi-kwadraattoets

Hierboven is uitgelegd hoe de $\chi^2$ statistiek kan worden berekend. Vaak zal men ook willen weten of de gevonden waarde statistisch significant is. Met andere woorden kunnen we met een bepaalde mate van zekerheid stellen dat de gevonden samenhang uit een populatie komt waar echt samenhang is of kan er sprake zijn van toevallige fluctuatie. 
Als $n$ voldoende groot is dan is de $\chi^2$ statistiek onder $H0$ (dus onder de aanname dat de rijen en kolommen onafhankelijk zijn) bij benadering verdeeld volgens de chi-kwadraatverdeling met vrijheidsgraden $df = (nr-1)(nk-1)$, waarbij $nr$ het aantal rijen is en $nk$ het aantal kolommen. Als de gevonden $\chi^2$ groter is dan een gekozen kritieke waarde uit deze verdeling dan kan deze waarde significant worden genoemd. Omdat de $\chi^2$ afhankelijk is van het aantal observaties en de grootte van de tabel, zegt deze toets niet zo veel bij grote tabellen. 

In dit geval geldt $\chi^2 =$ `r chisqr` ($df=$ `r df`), $p =$ `r round(pchisq(chisqr, df=df, lower.tail=FALSE),3)`.


## Visualiseren van de afhankelijkheid: correspondentieanalyse

Als er sprake lijkt van samenhang tussen de twee categorische variabelen, kan het interessant zijn een figuur te maken om deze samenhang te visualiseren. Dit kan door middel van een techniek genaamd correspondentieanalyse. Het idee achter correspondentieanalyse (CA) is een wiskundige techniek, genaamd singuliere waarden (value) decompositie (SVD), die de kruistabel ontbindt in scores voor rijen en scores voor kolommen in maximaal $k$ dimensies. Voor de visualisatie worden meestal alleen de eerste twee dimensies gebruikt. Per dimensie wordt ook een `eigenwaarde` (aangeduid met het symbool $\phi^2$) berekend, die een indicatie is hoe goed die dimensie de getallen in de tabel kan benaderen. Per definitie "verklaart" de eerste dimensie het meest, daarna de tweede enzovoort. Met $k$ dimensies kan de volledige tabel perfect worden gereconstrueerd. De eigenwaarden zijn gerelateerd aan de $\chi^2$. De som van de eigenwaarden is namelijk gelijk aan de $\chi^2$ gedeeld door $n$, in formule:

\begin{equation}
  \chi^2 = n\sum(\phi^2_k)
  (\#eq:kruistabellen-phi)
\end{equation}

De $\phi^2_k$ gedeeld door de som van alle $\phi^2_k$ geeft een percentage welke een maat is voor hoe goed de dimensie $k$ de samenhang in de tabel beschrijft. De visualisatie houdt in dat de categorieen van beide variabelen, de rijpunten en de kolompunten, worden weergegeven in een twee-dimensionele figuur. Met deze figuur kunnen bepaalde eigenschappen van de oorsprongelijke kruistabel worden gevisualiseerd. 

```{r kruistabellen-fig1, echo = F, message=FALSE, warning=FALSE, fig.cap='Samenhang in kruistabel gevisualiseerd.'}

  nd <- 2
  fit <- ca::ca(M, nd = nd)
  
  sqEigenValues <- round(100*fit$sv^2/(sum(fit$sv^2)),1)
  
  col_st <-  fit$colcoord %*% diag(fit$sv[1:nd])
  row_st <-  fit$rowcoord %*% diag(fit$sv[1:nd])

  coor <- as.data.frame(rbind(col_st, row_st))
  coor$type <- c(rep("partij",6),rep("media",5))
  dims <- paste0("dimensie",1:nd)
  colnames(coor) <- c(dims,"type")
  
  VVD1 <- round(coor["VVD","dimensie1"],3)
  VVD2 <- round(coor["VVD","dimensie2"],3)
  CDA1 <- round(coor["CDA","dimensie1"],3)
  CDA2 <- round(coor["CDA","dimensie2"],3)
  D1 <- abs(VVD1 - CDA1)
  D2 <- abs(VVD2 - CDA2)
  
  LVVDCDA <- round(sqrt(D1^2 + D2^2),2)

 ggplot2::ggplot(coor, ggplot2::aes(x=dimensie1, y=dimensie2, color = type)) + 
    ggplot2::geom_text(label=row.names(coor)) +
    ggplot2::theme(legend.position = "none") +
    ggplot2::geom_vline(xintercept = 0, linetype = "dashed") + 
    ggplot2::geom_hline(yintercept = 0, linetype = "dashed") +
    ggplot2::scale_x_continuous(limits = c(-.56, .70)) +
    ggplot2::scale_y_continuous(limits = c(-.56, .70)) 
  
  
```


Figuur \@ref(fig:kruistabellen-fig1) laat de oplossing van de eerste twee dimensies van een CA zien. De eerste dimensie "verklaart" $`r sqEigenValues[1]`\%$ en de tweede $`r sqEigenValues[2]`\%$ van de variatie in deze kruistabel. Categorieen van dezelfde variabele (dezelfde kleur) die dicht bij elkaar liggen hebben een vergelijkbaar profiel. GL en D66 zijn twee categorieen van de variable `politieke voorkeur` die dicht bij elkaar liggen. In de kruistabel kunnen we inderdaad zien dat beide partijen een vergelijkbaar profiel hebben met betrekking tot de media. CDA en PVV liggen ver uit elkaar op de eerste dimensie en hebben een van elkaar afwijkend profiel. Categorieen die dicht bij de oorsprong liggen hebben een gemiddeld profiel. In dit voorbeeld liggen de media Facebook and Online relatief dicht bij de oorsprong en hebben dus een niet heel extreem profiel met betrekking tot de politieke voorkeur. Het medium Krant daarentegen ligt vrij extreem en heeft dus een afwijkend patroon met betrekking tot politieke voorkeur.
  
De afstand tussen twee rijpunten (of twee kolompunten) in een kruistabel kunnen we definieren met de zogenaamde $\chi^2$ afstand. Dit is de afstand tussen twee rijprofielen (cq. kolomprofielen). Het rijprofiel van de $i^{de}$ rij, genoteerd als $p_i$, is:

\begin{equation}
  p_i = \frac{n_i} {\sum_j{(n_{ij})}}
  (\#eq:kruistabellen-rijprofiel)
\end{equation}

Hierbij is $n_i$ de $i^{de}$ rij uit de kruistabel. De som van de getallen in $p_i$ is per definitie gelijk aan 1.
De $\chi^2$ afstand tussen twee rijen ($i$ en $k$) is gedefineerd als:

\begin{equation}
  d_{ik} = \sum{\frac{(p_{i} - p_{k})^2} {r}}
  (\#eq:kruistabellen-afstand)
\end{equation}

De sommatie is over de elementen van $p$ en $r$, (b.v. $r=$  $`r round(arp,2)`$) . Op deze manier kunnen we voor iedere twee rijen en voor iedere twee kolommen een afstand uitrekenen. De afstanden tussen de politieke partijen staan in tabel \@ref(tab:kruistabellen-tab5). Op dezelfde wijze kan er een tabel met de onderlinge afstanden van de nieuwsmedia worden verkregen.

```{r kruistabellen-tab5, echo=FALSE, warning = FALSE, message=FALSE  }
 
dist.matrix <- function(data, average.profile){
  mat <- as.matrix(t(data))
  n <- ncol(mat)
  dist.mat<- matrix(NA, n, n)
  diag(dist.mat) <- 0
  for (i in 1:(n - 1)) {
    for (j in (i + 1):n) {
      d2 <- sum(((mat[, i] - mat[, j])^2) / average.profile)
      dist.mat[i, j] <- dist.mat[j, i] <- d2
    }
  }
  colnames(dist.mat) <- rownames(dist.mat) <- colnames(mat)
  dist.mat
}

tab <- dist.matrix(data= rp, average.profile = acp)
tab <- round(sqrt(tab),3)

distCDAVVD <- tab[3,4]

  md4e::knitTable(tab,     
                  caption = 'Chi-kwadraat afstanden tussen de politieke voorkeuren.')
                  
```

De $\chi^2$ afstand tussen bijvoorbeeld `VVD` en het `CDA` is $`r tab[3,4]`$. Een eigenschap van de visualtisatie van de correspondentieanalyse is dat de afstanden tussen  `politieke partijen` en tusssen de `nieuwsmedia` in de figuur een benadering zijn van de chi-kwadraat afstanden. De afstanden in de figuur zijn de "gewone" Euclidische afstanden, waarmee we in het dagelijks level ook afstanden meten. Deze afstand is met de coordinaten van beide partijen en via de stelling van Pythagoras eenvoudig te berekenen. Hoe meer dimensies we gebruiken, hoe beter de Euclidische afstand in de figuur de $\chi^2$ afstand van de kruistabel zal benaderen. Bij het maximaal aantal dimensies worden beide afstanden gelijk aan elkaar.

In figuur \@ref(fig:kruistabellen-fig2) is de afstand getoond. De afstand in de figuur tussen `CDA` en `VVD` op de eerste dimensie is $`r D1`$ en op de tweede dimensie $`r D2`$. Dit zijn de lengtes van de twee gestreepte lijntjes.  De wortel uit de som van deze kwadraten (Pythagoras) geeft de afstand tussen beide partijen: $d=$ $`r LVVDCDA`$. Dit is een benadering van de $\chi^2$ afstand $`r tab[3,4]`$. Zouden we de afstand in vier dimensies berekenen dan komt er precies $`r tab[3,4]`$ uit.



```{r kruistabellen-fig2, echo = F, message=FALSE, warning=FALSE, fig.cap='Afstand tussen punten in kruistabel gevisualiseerd.'}


ggplot2::ggplot(coor, ggplot2::aes(x=dimensie1, y=dimensie2, color = type)) + 
  ggplot2::geom_text(label=row.names(coor)) +
  ggplot2::theme(legend.position = "none") +
  ggplot2::geom_vline(xintercept = 0, linetype = "dashed") + 
  ggplot2::geom_hline(yintercept = 0, linetype = "dashed") +
  ggplot2::geom_segment(mapping=ggplot2::aes(x = coor["CDA","dimensie1"], y = coor["VVD","dimensie2"], 
                   xend = coor["CDA","dimensie1"], yend = coor["CDA","dimensie2"]),  colour = "black", linetype = "dashed") +
  ggplot2::geom_segment(mapping=ggplot2::aes(x = coor["CDA","dimensie1"], y = coor["VVD","dimensie2"],
                   xend = coor["VVD","dimensie1"], yend = coor["VVD","dimensie2"]), color = "black", linetype = "dashed") + 
  ggplot2::geom_segment(mapping=ggplot2::aes(x = coor["CDA","dimensie1"], y = coor["CDA","dimensie2"], 
                   xend = coor["VVD","dimensie1"], yend = coor["VVD","dimensie2"]), color = "blue") + 
  ggplot2::scale_x_continuous(limits = c(-.56, .70)) +
  ggplot2::scale_y_continuous(limits = c(-.56, .70)) +
  ggplot2::annotate("text",x = -0.16, y = -0.2, label = paste0("d= ",LVVDCDA), size = 3)
  
  
```

  
We kunnen ook de samenhang bekijken tussen categorieen van de verschillende variabelen. Als voorbeeld nemen we Twitter en de PVV. 
Allereerst trekken we pijlen vanuit de oorsprong naar deze punten, zie Figuur \@ref(fig:kruistabellen-fig3). De hoek, aangeduid met $r$, tussen deze pijlen is een maat voor de samenhang tussen deze categorieen. Het symbool $r$ wordt vaak gebruikt als symbool voor een correlatiecoefficient en ook in deze situatie kunnen we $r$ (of beter gezegd de cosinus van $r$) opvatten als een correlatie tussen de twee categorieen. Hoe kleiner de hoek wordt hoe groter de correlatie tussen beide punten. Bij een hoek van 90 graden (de cosinus is dan 0) is er geen samenhang tussen de punten.



```{r kruistabellen-fig3, echo = F, message=FALSE, warning=FALSE, fig.cap='Samenhang in kruistabel gevisualiseerd.'}


 ggplot2::ggplot(coor, ggplot2::aes(x=dimensie1, y=dimensie2, color = type)) + 
    ggplot2::geom_text(label=row.names(coor)) +
    ggplot2::theme(legend.position = "none") +
    ggplot2::geom_vline(xintercept = 0, linetype = "dashed") + 
    ggplot2::geom_hline(yintercept = 0, linetype = "dashed") +
    ggplot2::geom_segment(ggplot2::aes(x = 0, y = 0, xend = coor["PVV","dimensie1"], yend = coor["PVV","dimensie2"]),
                 arrow = ggplot2::arrow(length = ggplot2::unit(0.2, "cm")), color = "black") +
    ggplot2::geom_segment(ggplot2::aes(x = 0, y = 0, xend = coor["Twitter","dimensie1"], yend = coor["Twitter","dimensie2"]),
                 arrow = ggplot2::arrow(length = ggplot2::unit(0.2, "cm")), color = "black") + 
    ggplot2::geom_curve(ggplot2::aes(x = 0.16, xend = .14, y = 0.06, yend = .10), curvature = .65, color = "blue") +
    ggplot2::scale_x_continuous(limits = c(-.56, .70)) +
    ggplot2::scale_y_continuous(limits = c(-.56, .70)) +
    ggplot2::annotate("text", x = 0.19, y = 0.11, label = "r", size = 4)
  
  
```


## Verdieping: hoek tussen vectoren symboliseert samenhang

Wanneer we de samenhang tussen twee punten willen weten dan kunnen we die uitrekenen door gebruik te maken van enkele wiskundige stellingen. Allereerst gaan we de twee punten opvatten als vectoren vanuit de oorsprong. De twee pijlen zijn twee vectoren in een twee-dimensionele ruimte. 
In het algemeen geldt dat het inwendig product van twee vectoren, $a$ en $b$, op twee manieren kan worden gedefinieerd. De notatie van het inwendig product is $a'b$.

\begin{align*}
a'b & = ||a|| \times ||b|| \times cos(\alpha_{ab}) \\
a'b & = a_1b_1 + a_2b_2
\end{align*}

Hier geldt dat ||a|| en ||b|| de lengte van de vectoren representeren, en $\alpha_{ab}$ de hoek is tussen beide vectoren. De $a_1$ en $a_2$ zijn de coordinaten van punt $a$ op respectievelijk dimensie 1 en 2. Hetzelfde geldt voor $b_1$ en $b_2$.
Bovenstaande formules kunnen we combineren tot een uitdrukking voor de cosinus:

\begin{equation}
cos(\alpha_{ab}) = \frac{a_1b_1 + a_2b_2}{||a|| \times ||b|| }
  (\#eq:kruistabellen-cosinus)
\end{equation}

De teller in deze breuk geeft een som van de kruisproducten, dit is dus een maat voor de covariantie tussen twee variabelen.
De noemer geeft de lengte van de vectoren aan, dus de wortel uit de som van de kwadraten (stelling van Pythagoras) van de coordinaten. De lengte is dus een maat voor de standaardafwijking van een variabele.
  
De definitie van de correlatiecoefficient tussen $a$ en $b$ is als volgt:

\begin{equation}
\rho_{ab} = \frac{cov_{ab}}{\sigma_a \sigma_{b}}
  (\#eq:kruistabellen-rho)
\end{equation}

Hiermee is dus geillustreerd dat de cosinus van de hoek tussen twee vectoren gezien kan worden als de correlatie tussen deze vectoren.
