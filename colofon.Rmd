# Colofon {#colofon}


## Auteurs

Dit open Methodologie en Statistiek boek wordt gemaakt door medewerkers van de sectie Methoden en Technieken van de Vakgroep Theorie Methode en Statistiek aan de Faculteit Psychologie aan de Open Universiteit. 


<!-- Hieronder staan ze vermeld op alfabetische volgorde.  -->


<!-- **Jenny van Beek** -->

<!-- Als universitair docent ben ik sterk betrokken bij het onderwijs van de opleiding Psychologie. Ik ben docent bij alle onderzoekspractica van de opleiding Psychologie. Tevens ben ik examinator van de cursussen Inleiding in de Psychologie en Literatuurstudie. In mijn onderzoek richt ik me onder andere op de relatie tussen stemming en alcoholgebruik bij matige drinkers. Ik gebruik daarbij de experience sampling methode waarbij je deelnemers gedurende meerdere dagen, meerdere keren per dag een korte vragenlijst laat invullen. Ik kijk hierbij tevens naar de rol van reactiviteit: of de antwoorden beïnvloed raken door het vele invullen. -->


<!-- **Mira Duif** -->

<!-- Mijn werk bestaat uit onderwijs en onderzoek. Ik ben groot voorstander van afstandsonderwijs vanwege de kansen die het biedt. Ik vind uitdaging in het dusdanig online communiceren met studenten dat ze toch het gevoel hebben dat de docent nabij en betrokken is. Verder vind ik het erg leuk om de technologie te benutten voor het maken van cursusinhoud die overzichtelijk en ondersteunend is voor de student. Mijn interesses voor onderzoek zijn breed. Ik ben veel bezig met onderzoeksmethoden waarbij processen binnen individuen in kaart worden gebracht. De methodiek, de keuzes en beslissingen die hierbij genomen worden en de invloed daarvan, hebben mijn passie. Mijn promotieonderzoek betrof een ecological momentary assessment studie naar determinanten van alcoholgebruik bij de algemene bevolking.  -->


<!-- **Julia Fischmann** -->

<!-- Als docent ben ik betrokken bij het Methode & Technieken onderwijs van de bacheloropleiding psychologie. Naast begeleiding en beoordeling van studenten ben ik ook betrokken bij scripties en cursusontwikkelingen. Mijn passie en ambitie is om door duidelijke instructies op cursusniveau en door individuele feedback voor een dieper begrip van het hoe en waarom van onderzoeksmethoden te zorgen. Wat betreft onderzoek draag ik bij aan de ontwikkeling en evaluatie van serious games en ben ik geïnteresseerd in sociale netwerken en sociale steun. -->


<!-- **Rolf van Geel** -->

<!-- Is als universitair docent betrokken bij de begeleiding van alle onderzoekspractica in het Bachelor curriculum, de bachelorthesis en master scripties. Hij is examinator van de cursus longitudinaal onderzoek. -->


<!-- **Dirk Hoek** -->

<!-- Is als universitair docent betrokken bij de begeleiding van alle onderzoekspractica in het Bachelor curriculum, de bachelorthesis en master scripties. Hij is examinator van de cursus kwalitatief onderzoek. -->


<!-- **Natascha de Hoog** -->

<!-- Als universitair docent Methoden en Technieken ben ik betrokken bij het M&T-onderwijs van de bachelor Psychologie. Ik ben examinator van de cursus Inleiding Onderzoek en begeleid ook de overige onderzoekspractica, bachelor en master scripties. Mijn onderzoeksinteresse gaat uit naar de invloed van sociale identiteit op gezondheidsgedrag en welbevinden en meer algemeen naar gezondheidsvoorlichting, gedragsverandering en systematische reviews en meta-analyse. -->


<!-- **Inge Knippenberg** -->

<!-- Als docent houd ik mij bezig met het Methoden en Technieken-onderwijs van de Bachelor Psychologie. Mijn werkzaamheden bestaan voornamelijk uit het begeleiden van studenten en beoordelen van verslagen. Daarnaast ben ik werkzaam als promovenda bij het Radboudumc. Hier doe ik onderzoek naar doelbewuste en intuïtieve strategieën om depressie bij verpleeghuisbewoners te verminderen. -->


<!-- **Roeslan Leontjevas** -->

<!-- Ik ben examinator van Test- en Toetstheorie en voorzitter van de stagecommissie en van de Toetsingscommissie BAPD van de Open Universiteit. Ik begeleid zowel stagiaires,   scriptiestudenten als promovendi met de focus op het gebied van welzijn en psychosociale interventies bij fragiele patiënten waaronder mensen met dementie. Voorbeelden zijn een depressiebehandeling met zinvolle activiteiten en de rol van huisdieren in de langdurige zorg. Ik zet me in voor innovatieve designs (bijv. stepped-wedge) en analyses (bijv. Bayesiaanse analyses). -->


<!-- **Ron Pat-El** -->

<!-- Als universitair docent heeft mijn onderzoek betrekking op zowel onderwijskundige als methodologische thema’s. Binnen de methodologie gaat mijn interesse uit naar meetinvariantie, dus of vragenlijstscores tussen populaties cijfermatig vergeleken mogen worden, en naar agent-based models, dus hoe psychologische theorie middels computersimulaties getoetst kan worden. Binnen de onderwijskunde gaat mijn interesse uit naar het optimaliseren van formatieve toetspraktijken, en hoe serious games en chatbots daar een bijdrage aan kunnen leveren. Als docent ben ik actief in alle statistiekcursussen, en ik ben de examinator van Experimenteel Onderzoek. -->


<!-- **Gjalt-Jorn Peters** -->

<!-- Gjalt-Jorn Peters werkt bij Methodologie en Statistiek bij de faculteit Psychologie van de Open Universiteit. Hij geeft de onderzoekspractica en doet onderzoek naar zowel het verbeteren van de manier waarop we in de psychologie onderzoek doen als het verbeteren van de manier waarop gedragsveranderingsinterventies ontwikkeld worden. Hij schrijft een aantal Open Access boeken, zoals dit boek, het [Book of Behavior Change](https://bookofbehaviorchange), en het Reproducible Open Coding Kit boek oftewel het [ROCK boek](https://rockbook.org). Verder ontwikkelt hij een aantal R packages om onderzoekers en gedragsveranderingsprofessionals te ondersteunen zoals [`rock`](https://r-packages.gitlab.io/rock), [`behaviorchange`](https://r-packages.gitlab.io/behaviorchange), [`psyverse`](https://r-packages.gitlab.io/psyverse), en [`rosetta`](https://r-packages.gitlab.io/rosetta).  -->


<!-- **Peter Reniers** -->

<!-- Als promovendus werk ik aan het PET@home project. Voor dit project gebruiken we met name kwalitatieve methoden om inzicht te krijgen in de betekenis van huisdieren voor langdurige zorg cliënten, welke invloed huisdieren hebben op formele en informele zorgrelaties, en de ontwikkeling van hulpmiddelen voor zorgverleners die ondersteuning moeten bieden bij het nemen van beslissingen omtrent huisdieren van cliënten.  -->


<!-- **Piet van Tuijl** -->

<!-- Als docent ben ik betrokken bij de Methoden en Technieken vakken uit de bachelor. Tevens begeleid ik masterstudenten bij hun thesis en word ik geconsulteerd door medewerkers van de OU (veelal PhD's) voor vragen over onderzoeksmethoden en statistiek. Voor mij biedt vooral dit laatste de mogelijkheid om met veel verschillende onderzoeksterreinen in aanraking te komen en mijn passie voor onderzoek aan bod te laten komen.  Verder ben ik ook bezig met een eigen promotietraject bij de OU met als thema “Problematische Hyperseksualiteit”. -->


<!-- **Peter Verboon** -->

<!-- Peter Verboon is vakgroepvoorzitter van de vakgroep Methodologie en Statistiek bij de faculteit Psychologie van de Open Universiteit. Hij is verantwoordelijk voor de onderzoekspractica en doet onder andere onderzoek naar intensieve longitudinale designs, waaronder single case designs. Hij is auteur van het Open Access boek [Single Case Designs Analyses](https://scda). Verder ontwikkelt hij een aantal R packages om onderzoekers te ondersteunen zoals [`scda`](https://r-packages.gitlab.io/scda) en [`rosetta`](https://r-packages.gitlab.io/rosetta).  -->


<!-- **Tjeerd de Zeeuw** -->

<!-- Binnen het Extending the Earcheck project ontwikkel ik een interventie om gehoorbeschermend gedrag bij jongeren te bevorderen. Daarbij wordt gewerkt aan de hand van het Intervention Mapping protocol en worden een aantal recent ontwikkelde methodieken en (bijbehorende) R-packages gebruikt. Ik heb mijn master Psychologie behaald aan de Open Universiteit. Tevens ben ik afgestudeerd als gezondheidswetenschapper aan de Vrije Universiteit. Naast mijn promotieonderzoek doe ik onderzoek naar, en geef voorlichting over, het gebruik van prestatiebevorderende middelen in de sport bij Mainline in Amsterdam.  -->


## Datasets

De Palmer Penguins dataset is verzameld en beschikbaar gesteld door Kristen Gorman en Allison Horst [@palmerpenguins_key].

## Figuren

<!-- Cover -->

De cover afbeelding is CC0 oftewel Public Domain (gemaakt door Gjalt-Jorn Peters).

<!-- Hoofdstuk "Psychologie", 2 -->

Figuur \@ref(fig:psychologie-pavlovs-dog-conditioning) is CC-BY-SA, MaxxL (origineel: https://nl.m.wikipedia.org/wiki/Bestand:Pavlov%27s_dog_conditioning.svg).
 
<!-- Hoofdstuk "Data", 8 -->

Figuur \@ref(fig:datasets-ah1) is CC-BY, Allison Horst  (origineel: https://github.com/allisonhorst/stats-illustrations/blob/main/other-stats-artwork/not_normal.png).

Figuur \@ref(fig:datasets-allison-horst-coding-cases) is CC-BY, Allison Horst (origineel: https://github.com/allisonhorst/stats-illustrations/blob/master/other-stats-artwork/coding_cases.png).

<!-- Hoofdstuk "Verdelingsvormen en -maten", 10 -->

Figuur \@ref(fig:verdelingen-ah) is CC-BY, Allison Horst, (origineel: https://github.com/allisonhorst/stats-illustrations/blob/master/other-stats-artwork/summary_statistics.png).



## R packages

De volgende R-packages zijn gebruikt in dit boek:

```{r credits-r-packages, results='asis'}

packages <- c("bookdown", "ca", "effectsize", "GGally", "ggdist", "ggplot2", "ggrepel", "ggtext", "GPArotation", "gridExtra", "here", "jmvcore", "lme4", "lmerTest", "kableExtra", "knitr", "md4e", "palmerpenguins", "patchwork", "psych", "pwr", "remotes", "rmarkdown", "rosetta", "scda", "sn", "tinytex", "ufs", "viridis");

refs <-
  lapply(
    packages,
    function(pkg) {
      res <-
	      suppressWarnings(
          citation(
          pkg
        )
		  );
      res$key <- paste0(pkg, "_key");
      return(utils::toBibtex(res));
    }
  );

writeLines(
  paste0(unlist(refs), collapse="\n"),
  here::here("packages.bib")
);

cat(paste0("\n- `",
           packages,
           "` [@", packages, "_key]",
           collapse="\n"));

```

