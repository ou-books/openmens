
# Beschrijvingsmaten {#beschrijvingsmaten}
  
```{r beschrijvingsmaten-colofon, eval = FALSE, echo=FALSE, results="asis"}

md4e::knitWithClass(
  
  "Auteurs: Julia Fischmann, Gjalt-Jorn Peters en Natascha de Hoog; laatste update: 2024-10-25",
  
  class="colofon"
)

```

```{r beschrijvingsmaten-summary, eval = TRUE, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="In dit hoofdstuk wordt besproken:",
  
  "beschrijvingsmaten",
  "centrummaten",
  "spreidingsmaten",
  "uitschieters",
  "vrijheidsgraden.",
  
  class="overview"
)

```

```{r beschrijvingsmaten-courses, eval = TRUE, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="Deze stof wordt behandeld in de volgende Open Universiteitscursus(sen):",
  
  "Onderzoekspracticum inleiding onderzoek (PB0212)",
  
  class="courses"
)

```

```{r beschrijvingsmaten-dependencies, eval = FALSE, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="Dit hoofdstuk bouwt voort op deze andere hoofdstukken:",
  
  "Data",
  
  class="dependencies"
)

```


## Inleiding

Wanneer de dataverzameling van een onderzoek heeft plaatsgevonden en we beschikken over een databestand met alle gegevens, willen we vervolgens die gegevens op een zo overzichtelijke manier weergeven. Er zijn verschillende manieren om data samen te vatten in zogenaamde beschrijvingsmaten. Beschrijvingsmaten worden onderverdeeld in centrummaten en spreidingsmaten. Wat dit precies inhoudt wordt in dit hoofdstuk uiteengezet. 

## Centrummaten

De meest gangbare manier om data samen te vatten, is door middel van centrummaten. De meest gebruikte centrummaat in het dagelijkse leven is het *gemiddelde*. Andere belangrijke centrummaten zijn de *mediaan* en de *modus*. Deze maten geven op verschillende manieren het ‘centrum’ van een bepaalde datareeks aan. De berekeningen voor deze centrummaten verschillen van elkaar. Elke centrummaat geeft net een ander inzicht in de data en komt met zijn eigen voor- en nadelen.


### Het gemiddelde

Het gemiddelde is een centrummaat die we vaak in het dagelijkse leven terugzien. We praten bijvoorbeeld over de gemiddelde leeftijd van een groep, de gemiddelde lengte van Nederlanders of de gemiddelde wachttijd voor de uitslag van een toets. Het gemiddelde wordt berekend door alle getallen op te tellen en te delen door het aantal bij elkaar opgetelde getallen. Om de gemiddelde leeftijd van een groep te berekenen, moet je de leeftijd van alle deelnemers van een groep optellen en dat getal delen door het aantal deelnemers. Deze handeling kan worden beschreven met een formule.


\begin{equation}
\bar{x} = { \frac {{\sum_{i=1}^n} x_i} {n}}
(\#eq:beschrijvingsmaten-1)
\end{equation}

In deze formule staat $\bar{x}$ ($x$ onder een horizontale lijn) staat voor het gemiddelde, $x$ voor een datapunt, $n$ staat voor het aantal deelnemers en het somteken sigma ($\Sigma$, Griekse hoofdletter) staat voor 'Maak een som van alle datapunten van $1$ tot $n$'. Je ziet dat de som van alle datapunten in de formule gedeeld wordt door het aantal datapunten $n$.
  
Het gemiddelde wordt ook vaak gebruikt om de score van een individu op een vragenlijst over een psychologisch concept samen te vatten. Bijvoorbeeld, om burn-out te meten, beantwoorden deelnemers meerdere vragen die gaan over emotionele uitputting, vermoeidheid, hopeloosheid en cynisme op een schaal van $1$ (nooit) tot $5$ (altijd). Per deelnemer kan vervolgens een gemiddelde score op de burn-outschaal worden berekend door de scores van alle items op te tellen en te delen door het aantal items.
  
  
#### Outliers
  
Een *outlier*, ook wel *uitschieter* of *uitbijter* genoemd, is een extreem datapunt. In de meeste gevallen ligt de outlier dan ook ver af van de rest van de datapunten. Vaak is zo een extreme waarde een indicatie dat er een fout in de data zit. Er zijn echter ook outliers die bij de data horen, sommige deelnemers scoren nu eenmaal hoog of laag.
  
Stel, een vriendengroep (Monica [$35$] Rachel [$35$], Phoebe [$37$], Ross [$37$], Joey [$35$] en Chandler [$36$]) heeft een gemiddelde leeftijd van $35,8$ jaar. Joey heeft echter een fout gemaakt bij het opgeven van zijn leeftijd en heeft een nul te veel ingevuld ($350$). Zijn leeftijd ligt nu heel ver van de rest van de groep. De nieuwe gemiddelde leeftijd van de groep is $88,3$ jaar. Maar het is duidelijk dat de leeftijd van Joey een onmogelijke waarde betreft. Dit is een voorbeeld van een ongeldige extreme outlier. In een andere situatie brengt Rachel haar dochtertje Emma ($1$) mee naar een bijeenkomst van de vriendengroep. De leeftijd van Emma ligt ver van de rest van de groep. De nieuwe gemiddelde leeftijd is nu $30,9$ jaar. Maar in dit geval is de leeftijd van een realistische waarde, die ook bij de data hoort.
  
Het is steeds aan de onderzoeker om data op extreme waarden te controleren en te beslissen wat er met outliers moet gebeuren. Soms is het eenduidig dat outliers niet bij de data horen. In dat geval worden meestal de betreffende datapunten niet meegenomen in de analyses. Zelden is het mogelijk om de ‘ware’ waarde van deze datapunten te achterhalen. Lukt dit wel, dan is het goed om deze ‘ware’ waarden mee te nemen om zo dataverlies te voorkomen. Als outliers wel een geldige waarde betreffen, kan de onderzoeker beslissen om deze mee te nemen of om deze alsnog te verwijderen. Vaak wordt ervoor gekozen om de analyses twee keer uit te voeren, met en zonder de outliers. Zo kan bekeken worden hoe robuust de resultaten zijn, dat wil zeggen in hoeverre de resultaten door enkele extreme waarden beïnvloed worden.
  
De beslissing om outliers wel of niet mee te nemen in analyses heeft belangrijke gevolgen voor de resultaten. In bovenstaand voorbeeld heb je gezien dat een outlier het gemiddelde erg naar zich toetrekt. Er zijn ook centrummaten die minder gevoelig zijn voor outliers, met name de *modus* en de *mediaan*. Deze maten kunnen dus informatief zijn om data te beschrijven die een of meer outliers bevatten.
  
  
### De modus
  
De modus is gedefinieerd als de meest voorkomende waarde in de datareeks.
  
In het voorbeeld van de vriendengroep bestaande uit Monica ($35$), Rachel ($35$), Phoebe ($37$), Ross ($37$), Joey ($35$) en Chandler ($36$) is de modus, dus de meest voorkomende waarde, $35$. Deze komt drie keer voor. Deze waarde ligt dichtbij het gemiddelde van de vriendengroep zonder outliers, $35,8$.
  
De modus is echter minder gevoelig voor outliers dan het gemiddelde. Als Rachel haar dochtertje Emma ($1$) meeneemt, blijft de modus nog steeds $35$. Dit beschrijft de groep over het algemeen beter dan het nieuwe gemiddelde, $30,9$. Een bijzonder geval ontstaat als Joey zijn leeftijd per ongeluk invult als $350$ in plaats van $35$. In dat geval hebben de data twee modi, namelijk $37$ (komt twee keer voor) en $35$ (komt twee keer voor). Deze twee modi beschrijven de groep alsnog beter dan het nieuwe gemiddelde van $88,3$.
  
De modus is vooral informatief bij een beperkt aantal mogelijke waarden of bij een grote hoeveelheid datapunten. Als we bijvoorbeeld in deze vriendengroep de leeftijd in dagen zouden meten, zou elke deelnemer een verschillende waarde hebben en zou het niet heel informatief zijn om de meest voorkomende waarde te bepalen. Als er aan de andere kant duizenden deelnemers waren in deze leeftijdsrange, zou het waarschijnlijk wel mogelijk zijn om de modus van leeftijd in dagen te bepalen, omdat de kans groot is dat meerdere deelnemers op dezelfde dag geboren zijn.
  
  
### De mediaan
  
Een derde centrummaat is de mediaan. De mediaan is simpelweg het middelste datapunt in de datareeks. Om die te vinden worden eerst alle datapunten van laag naar hoog op een rijtje gezet.
  
$$35\quad 35\quad 35\quad 36\quad 37\quad 37\quad$$

Vervolgens wordt het middelste datapunt bepaald, dat wil zeggen het datapunt waar dezelfde hoeveelheid datapunten rechts (hoger) en links (lager) van liggen. In dit voorbeeld is er een even aantal datapunten. In dat geval wordt het gemiddelde van de middelste twee datapunten genomen.
  
$$(35+36)/2 = 35,5\quad $$
    
De mediaan ligt in dit geval, net als de modus, dichtbij de gemiddelde leeftijd van de vriendengroep zonder outliers, $35,8$. Ook de mediaan is minder gevoelig voor outliers dan het gemiddelde. Als Rachel haar dochtertje Emma meeneemt, ziet de geordende datareeks er als volgt uit.
  
$$1\quad 35\quad 35\quad 35\quad 36\quad 37\quad 37\quad$$
    
Het middelste datapunt, de mediaan, is in dit geval 35. Dit beschrijft de groep beter dan het nieuwe gemiddelde, $30,9$. Als Joey een foutje maakt en zijn leeftijd als $350$ invult, ziet de geordende datareeks er als volgt uit.
  
$$35\quad 35\quad 36\quad 37\quad 37\quad 350\quad$$
    
De mediaan is in dit geval $$(36+37)/2= 36.5\quad $$ 
    
Ook dit beschrijft de groep beter dan het nieuwe gemiddelde $88.3$.
  
  
## Spreidingsmaten 
  
De drie centrummaten (het gemiddelde, de modus, en de mediaan) kunnen worden gebruikt om data samen te vatten, maar ze geven nog niet voldoende informatie over de data. Laten we dit illustreren met een voorbeeld.
  
Stel je hebt twee groepen, een familie en een vriendengroep. De familie bestaat uit de kinderen Lily ($1$), Luke ($7$), Manny ($7$), Alex ($9$) en Hailey ($13$), de ouders Claire ($37$) en Phil ($37$), en Mitchell ($35$) en Cameron ($38$), de opa Jay ($63$) en zijn vrouw Gloria ($39$). De gemiddelde leeftijd van deze familie is $26$. 
  
De vriendengroep bestaat uit Robin ($25$), Lily ($26$), Ted ($27$), Barney ($27$), Marshal ($28$), Sheldon ($28$), Leonard ($27$), Howard ($26$), Rajesh ($27$), Penny ($24$), Bernadette ($25$) en Amy ($26$). De gemiddelde leeftijd van deze groep is $26,3$. 
  
De gemiddelden van deze twee groepen liggen heel dicht bij elkaar, maar als je de samenstelling bekijkt wordt duidelijk dat er een cruciaal verschil zit tussen de groepen. De leeftijden van de familie liggen ver uit elkaar (de jongste is $1$ en de oudste $63$) terwijl de leeftijden van de vriendengroep heel dicht bij elkaar liggen (de jongste is $24$ en de oudste $28$). In andere woorden, de spreiding van de datapunten verschilt substantieel.
  
Om een goed beeld van een datareeks te geven is het daarom noodzakelijk om naast een centrummaat ook de spreiding van de datapunten te rapporteren. Om dit te beschrijven zijn spreidingsmaten nodig.
  
  
### Range (bereik)
  
De eenvoudigste spreidingsmaat is de *range*, ook wel het *bereik*, van een variabele. Dit is simpelweg het verschil tussen het maximum en het minimum. 
  
In bovenstaand voorbeeld van de familie is het bereik van de leeftijd $$63-1= 62\quad$$

In de vriendengroep is het bereik $$28-24=4\quad$$
    
Maar de range is zeer gevoelig is voor outliers en volstaat daarom vaak niet om een goed beeld van de spreiding van de datapunten te geven. 
  
  
### Interkwartielafstand (IQR)
  
De interkwartielafstand (in het Engels de *interquartile range*, oftewel *IQR*) is voor spreidingsmaten wat de mediaan is voor centrummaten. Om de IQR te berekenen, worden de data weer geordend van laag naar hoog en vervolgens opgesplitst in vier kwartielen. Dan wordt bepaald bij welke waarde $25\%$ van de datapunten links liggen en $75\%$ van de datapunten rechts. Dit is het eerste kwartiel, ook wel het 25ste percentiel genoemd. Ook wordt bepaald bij welke waarde links en rechts $50%$ van de datapunten liggen. Dit is het tweede kwartiel, ook wel het 50ste percentiel genoemd. Dit is tevens de mediaan. Als laatste wordt bepaald bij welke waarde $75\%$ van de datapunten links en $25\%$ van de datapunten rechts liggen. Dit is het derde kwartiel, ook wel het 75ste percentiel genoemd.
  
Om de IQR uit het voorbeeld te berekenen worden de datapunten eerst weer gesorteerd van laag naar hoog.
  
$$\text{Familie:} \quad 1 \quad 7 \quad 7 \quad 9 \quad 13\quad 35\quad 37\quad 37\quad 38\quad 39\quad 63\quad$$
    
    
$$\text{Vrienden:} \quad 24\quad 25\quad 25\quad 26\quad 26\quad 26\quad 27\quad 27\quad 27\quad 27\quad 28\quad 28\quad$$
    
De datareeks wordt vervolgens opgedeeld in vier even grote delen met in totaal drie ‘breekpunten’, zoals weergeven in onderstaande figuur. Deze drie ‘breekpunten’ heten, van links naar rechts, het eerste kwartiel (ook wel Q1 genoemd), het tweede kwartiel (Q2, dit is gelijk aan de mediaan), en het derde kwartiel (Q3). De afstand tussen het eerste en het derde kwartiel heet de interkwartielafstand (IQR).(zie Figuur \@ref(fig:beschrijvingsmaten-A)).

```{r beschrijvingsmaten-A, fig.cap="Berekening van de interkwartielafstand.", echo=FALSE}
knitr::include_graphics(
  "img/interkwartielafstand.png"
)
```

De interkwartielafstanden voor ons voorbeeld zijn als volgt.
  
$$\text{Familie:} \quad Q1 = 7; Q2 = 35, Q3 = 38;\quad \text{Interkwartielafstand} = 38-7 = 31\quad$$
    
    
$$\text{Vrienden:} \quad Q1 = 25.5, Q2 = 26.5, Q3 = 27;\quad \text{Interkwartielafstand} = 27-25.5 = 1.5\quad$$
    
    
### Variatie
    
Een andere spreidingsmaat is de *variatie* oftewel *sum of squares* (*SS*). De sum of squares is de som van de gekwadrateerde afwijkingen van het gemiddelde.
  
Om de variatie te berekenen moet eerst voor elk datapunt worden bepaald hoe ver deze van het gemiddelde afwijkt. Als voorbeeld nemen we de familie. Hiervoor trekken wij het gemiddelde af van het datapunt (zie Tabel \@ref(tab:beschrijvingsmaten-1)).
  
```{r beschrijvingsmaten-1, message=FALSE, warning=FALSE, error=FALSE, cache=FALSE, fig.width=4, fig.height=4}
  
  dataFile <- "Family.csv";
  dat <- rosetta::getData(here::here("data", dataFile));
  
  vector <- dat$Age;
  meanX <- mean(dat$Age);
  
  md4e::knitTable(
    data.frame(
      Age = vector,
      mean = rep(meanX, length(vector)),
      differences = vector - meanX
    ),
    "De afwijking van het gemiddelde voor elk datapunt"
  );
  
```

Je ziet dat de datapunten die boven het gemiddelde liggen een positieve afwijking hebben en de datapunten die onder het gemiddelde liggen een negatieve afwijking. Om de afwijkingen op een zinvolle manier bij elkaar op te kunnen tellen, moeten deze allemaal positief zijn. Anders tellen ze op tot nul, omdat alle datapunten samen net zoveel positief als negatief afwijken van het gemiddelde. Door de afwijkingen te kwadrateren, krijgen ze allemaal een positieve waarde omdat een negatief getal keer zichzelf altijd een positief getal geeft. Deze gekwadrateerde afwijkingen van het gemiddelde worden ook afgekort tot kwadraten of ‘squares’ (zie Tabel \@ref(tab:beschrijvingsmaten-2)).
  
```{r beschrijvingsmaten-2, message=FALSE, warning=FALSE, error=FALSE, cache=FALSE, fig.width=4, fig.height=4}
  
  dataFile <- "Family.csv";
  dat <- rosetta::getData(here::here("data", dataFile));
  
  vector <- dat$Age;
  
  md4e::knitTable(
    data.frame(
      Age = vector,
      mean = rep(meanX, length(vector)),
      differences = vector - meanX,
      squares = (meanX - vector)^2
    ),
    "De afwijking van het gemiddelde voor elk datapunt met daarachter de gekwadrateerde afwijking"
  );
  
```
  
  
Om de sum of squares te berekenen, worden alle kwadraten bij elkaar opgeteld. Dit getal geeft de variatie in de data weer.
  
\begin{equation}
{`r paste0(round((vector - meanX)^2, 1), collapse="+")`} = `r round(sum((vector - meanX)^2), 1)`
(\#eq:beschrijvingsmaten-2)
\end{equation}
    
De sum of squares (SS) kunnen we in formulevorm als volgt schrijven.
    
\begin{equation}
\text{SS} = {\sum_{i=1}^n} (x_i - \overline{x})^2
(\#eq:beschrijvingsmaten-3)
\end{equation}
      
Het nadeel van de variatie is dat deze steeds groter wordt naarmate er datapunten bijkomen. Het betreft namelijk een som, waarbij steeds meer waarden bij elkaar opgeteld worden. Dat is onhandig, want de spreiding wordt niet noodzakelijk ook meer. Er kunnen namelijk datapunten bijkomen die heel dicht bij het gemiddelde liggen en toch wordt de variatie dan groter.
      
      
### Variantie
      
De *variantie* oftewel *mean squares* (*MS*) houdt rekening met het aantal datapunten en is daarom informatiever dan de Sum of Squares. 
      
Je hebt gezien hoe je de afwijking van het gemiddelde voor elke datapunt kunt berekenen en kwadrateren. Voor de sum of squares tel je deze kwadraten op. Voor de mean squares bereken je het gemiddelde van de kwadraten, dat wil zeggen de som gedeeld door het aantal observaties.
      
\begin{equation}
\frac {`r sum((vector - meanX)^2)`} {`r length(vector)`- 1} =  {`r round(var(vector), 1)`}
(\#eq:beschrijvingsmaten-4)
\end{equation}

De mean squares, oftewel variantie, wordt in de praktijk vaker gebruikt dan de variatie (sum of squares). Deze variantie is een handige maat voor spreiding, alhoewel deze niet op dezelfde schaal is als de datapunten in onze datareeks; alle waarden zijn namelijk eerst gekwadrateerd. De mean squares (MS) wordt weergegeven met de volgende formule.
        
\begin{equation}
{\text{MS}} = { \frac {{\sum_{i=1}^n} (x_i - \overline{x})^2} {n - 1}}
(\#eq:beschrijvingsmaten-5)
\end{equation}
      
          
#### Vrijheidsgraden
          
De noemer van deze formule, $n-1$, noemen we het aantal *vrijheidsgraden* van deze datareeks. Vrijheidsgraden, oftewel degrees of freedom (df) in het Engels, drukken uit hoeveel datapunten in een datareeks vrij kunnen variëren zonder dat de berekende statistiek verandert. Voor het gemiddelde van een datareeks zijn het aantal vrijheidsgraden n-1. Dat wil zeggen dat je in een datareeks alle datapunten behalve één willekeurige kunt veranderen. Dit ene datapunt moet een bepaalde waarde hebben om hetzelfde gemiddelde te behouden. 
          
Stel je hebt een datareeks van vier observaties met een gemiddelde van $2,5$. Dit betekent dat je $4-1 = 3$ vrijheidsgraden hebt. Je kunt dus drie observaties willekeurig kiezen, de vierde wordt altijd bepaald. Je kiest voor de eerste drie observaties de waardes $1$ $2$ $3$. De vierde observatie moet $4$ zijn om op een gemiddelde van $2,5$ uit te komen.
          
Stel je kiest voor de eerste drie observaties $0$ $0$ $0$. De vierde observatie moet dan $10$ zijn om het een gemiddelde van $2,5$ te behouden. Dit kun je zo vaak herhalen als je wilt. Je kunt bijvoorbeeld de willekeurige waarden kiezen $28$, $389$ en $964$. De vierde observatie moet dan $-1371$ zijn om op een gemiddelde van $2,5$ uit te komen.
          
Statistici zijn meestal niet geïnteresseerd in de individuele datapunten in een datareeks. Deze variëren sowieso door steekproeffout en meetfout. Het aantal vrijheidsgraden geeft aan hoeveel van deze datapunten er vrij kunnen veranderen zonder de essentie van de datareeks aan te tasten. Het aantal vrijheidsgraden verandert afhankelijk van hoeveel parameters de datareeks beschrijven. Hoe meer parameters je berekent, hoe minder waarden je willekeurig kunt aanpassen. Ingewikkelde statistische berekeningen vereisen daarom meer observaties.
          
          
### Standaarddeviatie
          
De meest gebruikte spreidingsmaat is de *standaardafwijking* oftewel de *standaarddeviatie* (*SD*). De standaardafwijking is de wortel van de variantie (mean squares) en geeft de gemiddelde afwijking van het gemiddelde weer. Door de wortel te trekken van de mean squares is de standaarddeviatie op dezelfde schaal als het gemiddelde en daardoor makkelijker te interpreteren.
          
Hieronder staat de formule voor de standaardafwijking. Je ziet dat deze gelijk is aan de formule voor de variantie, behalve dat er de wortel wordt getrokken.
          
\begin{equation}
\text{sd} = {\sqrt{\frac {{\sum_{i=1}^n} (x_i - \bar{x})^2} {n - 1}}}
(\#eq:beschrijvingsmaten-6)
\end{equation}

Als we deze formule invullen op basis van ons voorbeeld over de leeftijd van de familie, dan krijgen we het volgende. 

\begin{equation}
\text{sd} = {\sqrt{\frac {{\sum_{i=1}^n} (x_i - \bar{x})^2} {n - 1}}} = {\sqrt{\frac {{\sum_{i=1}^n} (x_i - \bar{x})^2} {\text{Df}}}} = {\sqrt{\frac {`r sum((meanX - vector)^2)`} {`r length(vector)` - 1}}} = \sqrt{`r round(var(vector), 1)`} = {`r round(sd(vector), 2)`}
(\#eq:beschrijvingsmaten-7)
\end{equation}
            

## Beschrijvingsmaten voor categorische variabelen
              
De centrum- en spreidingsmaten die te berekenen zijn, hangen af van de schaal waarop variabelen gemeten zijn. Bij categorische variabelen kan niet echt van een schaal gesproken worden. Maar, sommige centrummaten zijn bruikbaar. De modus, de meest voorkomende meetwaarde, kan ook bepaald worden bij ordinale en zelfs bij nominale variabelen. De mediaan is de meetwaarde van het middelste datapunt nadat alle datapunten van hoog naar laag (of andersom) zijn geordend. De mediaan kan dus ook bij ordinale variabelen bepaald worden. Toch geven de mediaan en modus maar relatief weinig informatie over de verdeling van een categorische variabele. Gelukkig is het juist bij categorische variabelen, omdat er tenslotte meestal maar relatief weinig categorieën zijn, eenvoudig te bepalen hoe vaak elke meetwaarde voorkomt. Dit heet een frequentieverdeling.
              
### Frequentieverdelingen
              
Een *frequentieverdeling* bestaat uit de *frequenties*, oftewel de aantallen, voor elke mogelijke meetwaarde. Dit is bijvoorbeeld de frequentieverdeling voor geslacht in een studie naar pinguïns op Antarctica, te zien in Tabel \@ref(tab:beschrijvingsmaten-3).
              
              
```{r beschrijvingsmaten-3, message=FALSE, warning=FALSE, error=FALSE, cache=FALSE, fig.height=4, fig.width=4}
              
              dataFile <- "Pinguins.csv";
              dat <- rosetta::getData(here::here("data", dataFile));
              md4e::knitTable(rosetta::freq(dat$sex)$dat,
                              caption="Frequenties van geslacht");
      
```
            
Zoals hier te zien, zaten er $165$ vrouwtjes en $168$ mannetjes in deze studie; oftewel $49,5\%$ vrouwtjes en $50,5\%$ mannetjes.
              
    
Deze *frequentietabel* heeft vier kolommen.
              
1. de frequenties, oftewel het aantal datapunten voor elke meetwaarde
2. het percentage voor elke meetwaarde van het totale aantal datapunten, waarbij de datapunten die geen meetwaarde hebben, zogenaamde *missing values*, ook in het totaal meetellen
3. het percentage voor elke meetwaarde van het (‘subtotale’) aantal datapunten waarvoor wel een meetwaarde bekend is
4. het *cumulatieve percentage* ten opzichte van dit laatste subtotaal
              
Een *cumulatief percentage* is het percentage van een bepaalde meetwaarde (of categorie) samen met de percentages van alle lagere (of, van de andere kant bekeken, hogere) meetwaarden (of categorieën). Het is nu misschien nog lastig te zien wat hier precies mee bedoeld wordt. Dit wordt duidelijker als we een frequentietabel bekijken van het jaar dat de pinguïns geobserveerd zijn. Deze frequentietabel vragen we getransponeerd op; dat wil zeggen dat de rijen en kolommen zijn omgedraaid. Dit is makkelijker voor studenten die schermlezers gebruiken, bijvoorbeeld slechtziende of blinde studenten,  te zien in Tabel \@ref(tab:beschrijvingsmaten-4).
              
```{r beschrijvingsmaten-4, message=FALSE, warning=FALSE, error=FALSE, cache=FALSE,fig.height=4, fig.width=4}
              
    md4e::knitTable(t(rosetta::freq(dat$year)$dat),
    caption="Frequenties van observatiejaar");
              
```
              
Het cumulatieve percentage voor $2007$ is $30,9\%$, want $103$ van de $333$ pinguïns zijn geobserveerd in $2007$. In $2008$ zijn $113$ pinguïns geobserveerd en dat is $33,9\%$. Het cumulatieve percentage voor $2008$ is $64,9\%$: de optelsom van $30,9\%$ en $33,9\%$. Dat cumulatieve percentage blijft op die manier de percentages optellen totdat 100% bereikt is. De frequenties in deze frequentietabellen heten ook wel de *absolute frequenties* om ze te onderscheiden van de percentages. Percentages worden ook wel *relatieve frequenties* genoemd.
              
Deze relatieve frequenties (de percentages, oftewel de proporties) geven informatie over het aantal datapunten in een categorie ten opzichte van het totale aantal datapunten. Die informatie is niet beschikbaar voor de absolute frequenties. Aan de andere kant geven de relatieve frequenties geen informatie over het aantal datapunten in de steekproef. Die informatie zit nu juist alleen in de absolute frequenties.
              
Voor een continue variabele is het overigens doorgaans niet praktisch om een frequentietabel op te vragen. Meestal hebben continue variabelen dermate veel verschillende meetwaarden dat het een hele lange lijst frequenties zou worden en dat elke meetwaarde bovendien bijna niet voorkomt.
              

In dit hoofdstuk gebruiken we de Palmer Penguins dataset ter illustratie. Deze dataset bevat informatie over drie pinguïnsoorten. Meer informatie is beschikbaar op https://allisonhorst.github.io/palmerpenguins.
