# Ontwerpen {#ontwerpen}


```{r ontwerpen-colofon, eval=TRUE, echo=FALSE, results="asis"}

md4e::knitWithClass(
  
  "Auteur: Gjalt-Jorn Peters en Natascha de Hoog; laatste update: 2023-02-10",

  class="colofon"
)

```

```{r ontwerpen-summary, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="In dit hoofdstuk wordt besproken:",
  
  "steekproeven",
  "kwalitatief onderzoek",
  "cross-sectioneel onderzoek",
  "experimenteel onderzoek",
  "longitudinaal onderzoek",
  "pilot studies.",

  class="overview"
)

```

```{r ontwerpen-courses, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="Deze stof wordt behandeld in de volgende Open Universiteitscursus(sen):",
  
  "Onderzoekspracticum inleiding onderzoek (PB0212)",
  

  class="courses"
)

```

```{r ontwerpen-dependencies, eval=FALSE, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="Dit hoofdstuk bouwt voort op deze andere hoofdstukken:",
  
  "Constructen",
 
  class="dependencies"
)

```

## Inleiding

<!-- Dit hoofdstuk nog vullen met 1.4 en 2.2 uit Inleiding onderzoek. -->

Het studieontwerp of design bepaalt de manier waarop meetinstrumenten en eventuele manipulaties worden toegepast. Het studieontwerp is dus een kader voor de gehele procedure van de studie. 

<!-- Specifieke ontwerpen en voor/nadelen bespreken -->

<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->

## Steekproeven

In wetenschappelijk onderzoek kan in principe nooit de hele wereld onderzocht worden: studies zijn beperkt in de beschikbare middelen zoals tijd en geld. Daarom wordt praktisch altijd een steekproef genomen: een selectie uit de hele wereld.

Bij wetenschappelijk onderzoek gebruiken we de term *populatie* voor 'de hele wereld' – voor zover deze relevant is voor de studie en uit die populatie wordt een *steekproef* geselecteerd.

Als die selectie helemaal willekeurig is en elk lid van de populatie dus evenveel kans heeft om in die steekproef terecht te komen, dan wordt de steekproef *aselect* genoemd. Omdat aselecte steekproeven door toeval tot stand komen, is het mogelijk om kansrekening te gebruiken om alsnog uitspraken te doen over de populatie.

Soms zijn aselecte steekproeven niet mogelijk of niet wenselijk.

```{r ontwerpen-notice-1, echo=FALSE, results="asis"}

md4e::knitWithClass(
  
  "Deze sectie moet nog worden uitgebreid. Dit zal (uiterlijk) gebeuren als de betreffende stof wordt gebruikt in een cursus; dit is waarschijnlijk de volgende revisie van Onderzoekspracticum kwalitatief onderzoek.",

  class="notice"
)

```

<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->

## Kwalitatief onderzoek

```{r ontwerpen-notice-2, echo=FALSE, results="asis"}

md4e::knitWithClass(
  
  "Deze sectie moet nog worden geschreven. Dit zal (uiterlijk) gebeuren als de betreffende stof wordt gebruikt in een cursus; dit is waarschijnlijk de volgende revisie van Onderzoekspracticum kwalitatief onderzoek.",

  class="notice"
)

```

<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->

## Cross-sectioneel onderzoek

```{r ontwerpen-notice-3, echo=FALSE, results="asis"}

md4e::knitWithClass(
  
  "Deze sectie moet nog worden geschreven. Dit zal (uiterlijk) gebeuren in september 2020.",

  class="notice"
)

```

<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->

## Experimenteel onderzoek

```{r ontwerpen-notice-4, echo=FALSE, results="asis"}

md4e::knitWithClass(
  
  "Deze sectie moet nog worden geschreven. Dit zal (uiterlijk) gebeuren als de betreffende stof wordt gebruikt in een cursus; dit is waarschijnlijk de volgende revisie van Onderzoekspracticum experimenteel onderzoek.",

  class="notice"
)

```

<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->

## Longitudinaal onderzoek

```{r ontwerpen-notice-5, echo=FALSE, results="asis"}

md4e::knitWithClass(
  
  "Deze sectie moet nog worden geschreven. Dit zal (uiterlijk) gebeuren als de betreffende stof wordt gebruikt in een cursus; dit is waarschijnlijk de volgende revisie van Onderzoekspracticum experimenteel onderzoek.",

  class="notice"
)

```

<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->

## Pilotstudies {#ontwerpen-pilot-studies}

Pilotstudies zijn een cruciaal onderdeel van wetenschappelijk onderzoek. Ze kunnen helpen om fatale schendingen van de interne validiteit van een studie tijdig te detecteren (zie voor meer informatie over de interne validiteit het hoofdstuk [Validiteit van Ontwerpen](https://openmens.nl/validiteit-van-ontwerpen).

<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->

### Benodigde steekproef voor een pilotstudie

Wolfgang Viechtbauer en collega's hebben een gratis beschikbare tool ontwikkeld om te berekenen hoeveel deelnemers nodig zijn voor een pilotstudie [@viechtbauer_simple_2015]. Deze is beschikbaar op http://pilotsamplesize.com.

<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->

### Pilotstudies om effectgroottes te schatten

Wanneer het doel van een pilotstudie is om een effectgrootte te schatten, zijn er beduidend meer deelnemers nodig [zie ook @kraemer_caution_2006]. Bijvoorbeeld, om te schatten wat Cohen's $d$ is, zijn ongeveer $1000$ deelnemers nodig. De tabellen in het hoofdstuk [Power voor t-toetsen](https://openmens.nl/tpower) kunnen gebruikt worden om een grove schatting te maken van het aantal benodigde deelnemers. Om het exacte aantal benodigde deelnemers te berekenen, kan het hoofdstuk "[Cohen’s d: accuracy](https://sci-ops.gitlab.io/rosetta-stats/cohens-d-accuracy.html) uit Rosetta Stats worden geraadpleegd.

