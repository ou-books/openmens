#!/bin/sh

OUT_DIR="$1"
INITIAL_BRANCH=$(git rev-parse --abbrev-ref HEAD)

pip install mkdocs
mkdir -p $OUT_DIR

# It is important to start with master branch:
git checkout $BRANCH
mkdocs build --strict --site-dir $OUT_DIR/

# Generating documentation for each other branch in a subdirectory
for BRANCH in $(git branch --remotes --format '%(refname:lstrip=3)' | grep -Ev '^(HEAD|master)$'); do
    git checkout $BRANCH
    mkdocs build --strict --site-dir $OUT_DIR/$BRANCH
done

git checkout $INITIAL_BRANCH
