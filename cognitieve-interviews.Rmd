# Cognitieve Interviews {#cognitieve-interviews}

Het cognitieve interview is een kwalitatieve onderzoeksmethode die fundamenteel verschilt van het meeste andere kwalitatieve onderzoek. Dit komt omdat het object van onderzoek niet de mens of de menselijke psychologie is, maar een specifieke uitwerking van een operationalisatie van een construct, dus een meetinstrument of een manipulatie (zie voor meer achtergrond de hoofdstukken [Psychologie](https://openmens.nl/psychologie), [Stimuli](https://openmens.nl/stimuli), [Constructen](https://openmens.nl/constructen), en [Constructen Meten](https://openmens.nl/constructen-meten)).

Er worden wel mensen gebruikt om die operationalisatie te onderzoeken, maar als middelen in het onderzoek. De kwalitatieve dataverzameling wordt dus niet gebruikt om informatie over de deelnemers te verzamelen, maar om informatie over de operationalisatie te verzamelen.

Dit betekent dat als er wordt doorgevraagd, het doel niet is om aspecten van de menselijke psychologie in kaart te brengen. In plaats daarvan is het doel om in kaart te brengen hoe stimuli, methoden om responsen te registeren, en de procedure waarmee die worden gecombineerd worden geïnterpreteerd.

Er worden daarom ook geen topiclijsten of interviewschema's gebruikt. In plaats daarvan worden twee technieken gecombineerd: de *think-aloud*-methode en *probing*. De think-aloud-methode bestaat eruit dat de deelnemers worden geïnstrueerd om hardop na te denken. Veel dingen die normaal impliciet blijven worden dan beter observeerbaar. *Probing* is vergelijkbaar met de rol die doorvragen heeft in kwalitatief onderzoek: er worden specifieke vragen gesteld om dieper in te gaan op een gegeven aspect van een operationalisatie.

```{r cognitieve-interviews-notice-1, echo=FALSE, results="asis"}

md4e::knitWithClass(
  
  "Deze sectie moet nog worden geschreven. Dit zal (uiterlijk) gebeuren als de betreffende stof wordt gebruikt in een cursus; dit is waarschijnlijk de volgende revisie van Onderzoekspracticum kwalitatief onderzoek.",

  class="notice"
)

```
