---
output: html_document
editor_options: 
  chunk_output_type: console
---

# Data {#datasets}

<!-- Dit hoofdstuk is een copy van een oude versie van het data hoofdstuk zodat onderstaande code bewaard bleef. -->

<!-- NB: Dit is data embedded volgens de bookmark standaard. -->
<!--     Dit is om te testen - graag laten staan voor nu!    -->

<!--[BOOKMARK::author::name::Gjalt-Jorn Peters]-->

<!--[BOOKMARK::dates::lastUpdate::2022-11-03]-->

<!--[BOOKMARK::metadata::topic::datapunten]-->
<!--[BOOKMARK::metadata::topic::datareeksen]-->
<!--[BOOKMARK::metadata::topic::datatypen en meetniveaus]-->
<!--[BOOKMARK::metadata::topic::metadata]-->
<!--[BOOKMARK::metadata::topic::datasets]-->
<!--[BOOKMARK::metadata::topic::variabelenamen]-->
<!--[BOOKMARK::metadata::topic::bestandsformaten en -namen]-->

<!--[BOOKMARK::metadata::course_id::PB0812]-->
<!--[BOOKMARK::metadata::course_id::PB0412]-->

<!--[BOOKMARK::metadata::dependency:: ]-->

<!--[BOOKMARK-FUNCTION-LOCAL::md4e_chapter_colofon]-->

<!--[BOOKMARK-FUNCTION-LOCAL::md4e_chapter_topics]-->

<!--[BOOKMARK-FUNCTION-LOCAL::md4e_chapter_courses]-->

<!--[BOOKMARK-FUNCTION-LOCAL::md4e_chapter_dependencies]-->


```{r datasets-colofon, eval=FALSE, echo=FALSE, results="asis"}

md4e::knitWithClass(
  
  "Auteur: Gjalt-Jorn Peters en Natascha de Hoog; Laatste update: 2023-04-14",

  class="colofon"
)

```

```{r datasets-summary, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="In dit hoofdstuk wordt besproken:",
  
  "datapunten",
  "datareeksen",
  "meetniveau's",
  "datasets",
  "variabelenamen.",
  
  class="overview"
)

```

```{r datasets-courses, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="Deze stof wordt behandeld in de volgende Open Universiteitscursus(sen):",
  
  "Onderzoekspracticum inleiding onderzoek (PB0212)",
  "Onderzoekspracticum cross-sectioneel onderzoek (PB0812)",
  
  class="courses"
)

```


```{r datasets-dependencies, echo=FALSE, results="asis"}

# md4e::knitListWithClass(
#   
#   prefix="Dit hoofdstuk bouwt voort op deze andere hoofdstukken:",
#   
#   "Datasets",
#   
#   class="dependencies"
# )

```

## Inleiding {#datasets-inleiding}

Als we het hebben over kwantitatieve data, dus data verkregen uit bijvoorbeeld cross-sectioneel of experimenteel onderzoek, dan zijn er een aantal begrippen die steeds terugkomen en waar we op moeten letten bij het uitvoeren en analyseren van het onderzoek. In dit hoofdstuk worden een aantal aspecten van kwantitatieve data besproken, waaronder datapunten, meetniveaus en variabele namen.


## Datapunten {#datasets-datapunten}

Een datapunt is een representatie van de uitkomst van een meting. Zo'n meting kan allerlei vormen aannemen:

-   een geboortedatum
-   een voornaam
-   de respons op een item
-   het aggregaat van meerdere responsen (zoals een gemiddelde)
-   de codering van een videofragment door een onderzoeker
-   de categorisatie van iemands antwoord in een interview
-   een foto
-   een audiofragment

Datapunten worden achter de schermen opgeslagen als reeksen nullen en enen (bits). Er zijn conventies over hoe een patroon aan nullen en enen een getal, een letter, of een ander karakter representeert. Op diezelfde manier worden vaak getallen gebruikt om datapunten te representeren die voor een categorie staan. Iemands haarkleur kan bijvoorbeeld worden gecodeerd als $1$, $2$, $3$ of $4$, waarbij elk getal dan voor een andere haarkleur staat.

## Datareeksen {#datasets-datareeksen}

In wetenschappelijk onderzoek worden meestal meerdere datapunten verzameld. Er worden bijvoorbeeld meerdere onderzoekseenheden onderzocht (in de psychologie zijn dat meestal mensen) of er worden meerdere data verzameld bij een onderzoekeenheid, of allebei. Een reeks van meerdere datapunten die hetzelfde representeren heet een datareeks.

Als bijvoorbeeld bij vier deelnemers hun leeftijd wordt geregistreerd, is er een datareeks van vier datapunten beschikbaar. Een datareeks bestaat in de onderzoekspraktijk overigens vaak al snel uit honderden datapunten. 

## Meetniveaus {#datasets-meetniveaus}

Datapunten en datareeksen worden onderverdeeld in verschillende variabelen. Een variabele is bijvoorbeeld leeftijd of experimentele conditie. Variabelen kunnen verschillende meetniveaus hebben.

Veel variabelen zijn *continu*: ze zijn meetbaar op een ononderbroken schaal en kunnen in de populatie oneindig veel waarden aannemen. Enkele voorbeelden zijn leeftijd, lengte of een score op een Likertschaal. Continue variabelen zijn van het hoogste *meetniveau*, maar er bestaan ook variabelen van een lager meetniveau. 

**Nominaal**

Geslacht is een voorbeeld van een variabele van het laagste meetniveau: *nominaal*. Geslacht bestaat traditioneel uit twee categorieën: man en vrouw. Deze categorieën zijn niet te ordenen en er kan ook niet mee gerekend worden. Geslacht is in dit geval een voorbeeld van een *dichotome*, oftewel een *binaire*, variabele. Een dichotome variabele is een nominale variabele die maar twee waarden kan aannemen. 

Een ander voorbeeld van een variabele met een laag meetniveau is haarkleur. Haarkleur kan zwart, bruin, blond, rood, grijs of wit zijn. Maar zwart is niet ‘hoger’ of ‘lager’ dan rood. Deze categorieën zijn alleen te benoemen, maar niet te ordenen. We noemen dit meetniveau daarom *nominaal*. 

**Ordinaal**

Opleidingsniveaus zijn wel te ordenen. Iemand die alleen de basisschool heeft gedaan, heeft een lager opleidingsniveau dan iemand die ook de middelbare school heeft afgemaakt. Iemand die ook nog een vervolgopleiding afrondt, heeft een nog hoger opleidingsniveau. Bovendien zijn er binnen elke fase vaak ook nog niveaus te onderscheiden, zoals vmbo, havo en vwo in Nederland. 

Hoewel deze categorieën ten opzichte van elkaar te ordenen zijn, geldt niet dat iemand die een universitaire studie gedaan heeft, ‘dubbel zo’n hoog opleidingsniveau’ heeft als iemand die alleen de middelbare school heeft afgerond. De afstand tussen de geordende categorieën is onbekend: we kunnen de categorieën alleen maar ordenen. We noemen dit daarom een *ordinale* variabele. Omdat ordinale variabelen wel van hoog naar laag te ordenen zijn, hebben ze een hoger meetniveau dan nominale variabelen (zie Figuur \@ref(fig:datasets-ah1)).

```{r datasets-ah1, fig.cap="Tekening  van nominale, ordinale en binaire data. Artwork door Allison Horst.", echo=FALSE}
knitr::include_graphics(
  "img/nominal_ordinal_binary.png"
)
```

Samen heten nominale en ordinale variabelen *categorische* of *discrete* variabelen. De verschillende *meetwaarden* die deze variabelen kunnen aannemen zijn altijd categorieën, zoals ‘vrouw’, ‘MBO’, ‘minderjarig’ of ‘40-50 jaar’.

**Continue variabelen**

Tegenover categorische variabelen staan *continue* variabelen. Deze variabelen kunnen in theorie alle denkbare meetwaarden aannemen, meestal op een schaal van ‘min oneindig’ tot ‘plus oneindig’, waarbij waarden steeds onwaarschijnlijker worden naarmate ze verder van het gemiddelde af liggen. Dit geldt niet voor alle variabelen: lengte kan bijvoorbeeld niet negatief zijn.

Binnen de continue variabelen worden soms twee meetniveaus onderscheiden: het *interval* niveau en het *ratio* niveau. Het verschil tussen deze twee meetniveaus is het al dan niet bestaan van een zogenaamd ‘absoluut nulpunt’, waardoor er wel of niet een verhouding tussen twee getallen uitgedrukt kan worden. Lengte is bijvoorbeeld een variabele op rationiveau. Een lengte van $0$ betekent de afwezigheid van lengte en een lengte van $2$ meter is twee keer zo lang als een lengte van $1$ meter. 

Temperatuur is daarentegen een voorbeeld van een variabele op interval niveau. Een temperatuur van $0$ graden geeft wel degelijk een temperatuur aan. Ook kun je niet zeggen dat $20$ graden dubbel zo warm is als $10$ graden. Wel zijn op het intervalniveau de intervallen tussen opeenvolgende meetwaarden altijd even groot. Ook constructen gemeten op Likertschalen en dus veel psychologische constructen hebben een interval meetniveau. 

Voor de toepassing van statistiek is het onderscheid tussen het interval- en het rationiveau niet belangrijk. Het is enkel belangrijk dat ze beide continu zijn, want met continue variabelen kan gerekend worden. Twee continue variabelen kunnen, als ze op dezelfde schaal gemeten zijn, worden opgeteld of gemiddeld om een betekenisvol resultaat te verkrijgen.

Er zijn dus continue variabelen, waarbij het onderscheid tussen interval- en ratiovariabelen niet relevant is, en categorische (of discrete) variabelen, waarbij het onderscheid tussen nominaal en ordinaal wel relevant is.

### Afwegingen bij meetniveaus {#datasets-afwegingen-bij-meetniveaus}

Variabelen hebben niet altijd een vast meetniveau, het meetniveau van een variabele is een keuze die de onderzoeker maakt tijdens het operationaliseren. Leeftijd in jaren is bijvoorbeeld een intervalvariabele, maar wordt soms als ordinale variabele gemeten, bijvoorbeeld met de antwoordopties ‘jonger dan 18’, ‘18 tot 35’, ‘35 tot 50’, ‘50 tot 65’ en ‘ouder dan 65’. Meetniveaus zijn vaak dus niet zozeer eigenschappen van variabelen ‘in de realiteit’, maar kenmerken van operationalisaties, oftewel van meetinstrumenten of manipulaties.

Kiezen voor een categorisch meetniveau terwijl een variabele op een continu niveau gemeten kan worden, kan schadelijk zijn voor het onderzoek om vier redenen.

1. Er zijn altijd meer deelnemers nodig naarmate het meetniveau van de betreffende variabelen lager is. Een verband aantonen tussen twee continue variabelen vereist  minder deelnemers dan wanneer een van de variabelen categorisch is, laat staan als beide variabelen categorisch zijn.
2. Veel variabelen die we willen meten in onderzoek zijn continu. Daar waar mensen categorieën waarnemen, blijkt na nader onderzoek meestal dat er in feite sprake is van een of meer onderliggende continue variabelen, die mensen min of meer arbitrair in groepen indelen. Categorische operationalisaties zijn dus niet altijd valide.
3. Het is altijd mogelijk om van een continue variabele terug te gaan naar lagere niveaus, maar niet andersom. Als een deelnemer '35 tot 50' aankruist, is onbekend of de leeftijd $36$ is of $47$.
4. Groepen mensen bestaan vaak niet uit duidelijk onderscheidbare subgroepen. Elke indeling in categorieën geeft dus vaak een vertekening van de werkelijkheid. Het meten van variabelen op een categorisch meetniveau vereist namelijk dat harde grenswaarden, zogenaamde ‘cut-offs’, worden gekozen. Om het leeftijdsvoorbeeld weer te gebruiken: je neemt hierbij aan dat iemand van $36$ veel meer lijkt op iemand van $49$ dan op iemand van $34$. Als dit niet zo is, is een cut-off van $35$ niet goed te verdedigen.

Bovenstaande neemt niet weg dat er best wat situaties zijn waarbij we onderzoek doen met categorische variabelen. Ten eerste resulteren manipulaties in experimenteel onderzoek bijna altijd in categorische variabelen. Stel een onderzoeker kijkt naar het effect van stemming op welbevinden, waarbij stemming gemanipuleerd wordt door iemand zich positief of juist negatief te laten voelen. Stemming is in dit geval een categorische variabele. Ten tweede moet bij de ontwikkeling van meetinstrumenten niet alleen gelet worden op het optimale meetniveau. Een operationalisatie moet bovenal valide en betrouwbaar zijn. Dit betekent onder andere dat die goed aan moet sluiten op de belevingswereld van deelnemers, dus van de te onderzoeken populatie.

Het is dus belangrijk categorische en continue variabelen te onderscheiden en op de juiste manier te behandelen. 


## Datasets {#datasets-datasets}

Een dataset is een verzameling datareeksen die zo zijn georganiseerd dat duidelijk is welke data bij elkaar horen. Meestal gebeurt dit door de data in tabelvorm op te slaan, waarbij elke kolom van de tabel een datareeks is, en de data in elke rij op een bepaalde manier samenhangen.

Een veelgebruikt formaat voor datasets is bijvoorbeeld dat alle datapunten van een gegeven onderzoeksdeelnemer in dezelfde rij staan. Elke kolom van de dataset representeert dan een variabele, elke rij een deelnemer, en elke cel een datapunt van een bepaalde variabele voor een bepaalde deelnemer. Dit is hoe de meeste datasets die je in de verschillende cursussen gebruikt eruit zien.

Soms wordt er herhaaldelijk data verzameld voor een deelnemer, bijvoorbeeld als er een serie reactietijden wordt verzameld. In die gevallen is het juist gangbaar om elke reactietijd op een aparte rij te plaatsen, en heeft elke deelnemer een eigen bestand. Deze datasets kunnen onder elkaar worden geplakt, en dan wordt een extra kolom toegevoegd om aan te geven over welke deelnemer elke regel gaat. In dat geval bevat de uiteindelijke samengevoegde dataset dus meer regels dan er deelnemers zijn. Ook komt het voor dat de reactietijden in aparte kolommen worden geplaatst, en worden genummerd. In dat geval bevat elke regel nog steeds data over een deelnemer, en staat het aantal regels gelijk aan het aantal deelnemers.

## Variabelenamen {#datasets-variabelenamen}

Het is belangrijk om goed na te denken over de variabelenamen die je kiest voor je variabelen. Variabelenamen spreken zoveel mogelijk voor zichzelf: vermijd dus cryptische termen en afkortingen. Gebruik ook Engels voor variabelenamen, zodat de dataset voor iedereen toegankelijk is (Engels is zowel de taal van de wetenschap als de taal die de meeste mensen kunnen lezen). Verder is het belangrijk om alleen karakters te gebruiken die op alle computers en statistische programma's goed gelezen kunnen worden; spaties kunnen niet gebruikt worden. Tot slot is het slim om te anticiperen op machine-readability van variabelenamen door systematische patronen te gebruiken om de variabelenamen mee op te bouwen.

### Karakters in variabelenamen {#datasets-karakters-in-variabelenamen}

De volgende karakters kunnen 'veilig' worden gebruikt in variabelenamen:

-   kleine letters (`a`, `b`, `c`, `d`, `e`, `f`, `g`, `h`, `i`, `j`, `k`, `l`, `m`, `n`, `o`, `p`, `q`, `r`, `s`, `t`, `u`, `v`, `w`, `x`, `y` & `z`);
-   hoofdletters (`A`, `B`, `C`, `D`, `E`, `F`, `G`, `H`, `I`, `J`, `K`, `L`, `M`, `N`, `O`, `P`, `Q`, `R`, `S`, `T`, `U`, `V`, `W`, `X`, `Y` & `Z`);
-   cijfers (`0`, `1`, `2`, `3`, `4`, `5`, `6`, `7`, `8` & `9`);
-   laag liggend streepje (`_`);
-   punt (`.`)

Andere karakters kunnen een speciale betekenis hebben in sommige statistische programma's en moet je dus vermijden. Naast deze lijst van karakters is er nog een beperking: variabelenamen kun je het beste laten beginnen met een letter. Een laag liggend streepje levert ook zelden problemen op, maar variabelenamen kunnen nooit starten met een cijfer, en een punt kun je ook beter vermijden.

### Kamelen en slangen {#datasets-kamelen-en-slangen}

Goede variabelenamen zijn dus Engelstalig, spreken voor zichzelf en gebruiken geen problematische karakters. Om toch woorden in een variabelenaam te kunnen onderscheiden wordt bijvoorbeeld gebruikgemaakt van camelCase (in plaats van een spatie is de eerste letter van een nieuw woord een hoofdletter) of snake_case (spaties worden vervangen door laag liggende streepjes) (zie Figuur \@ref(fig:datasets-allison-horst-coding-cases)).

```{r datasets-allison-horst-coding-cases, fig.cap="Illustratie van de verschillende 'cases'. Artwork door Allison Horst.", echo=FALSE}
knitr::include_graphics(
  "img/datasets-allison-horst-coding-cases.png"
)
```

### Machine-readability {#datasets-machine-readability}

Bovenstaande richtlijnen zorgen ervoor dat variabelenamen op alle platforms (Windows, MacOS, Linux) en in alle statistische programma's (jamovi, R, SPSS -- en andere programma's zoals JASP of Excel) geopend kunnen worden, en dat de kans groot is dat iedereen ze begrijpt omdat een passende omschrijving in het Engels is gebruikt.

Er is nog een laatste richtlijn voor goede variabelenamen, en dat is dat ze ook zo goed mogelijk te begrijpen zijn voor computers. Dit heet 'machine-readability' en bereik je door patronen te gebruiken waardoor de variabelenamen herkenbaar worden.

Neem bijvoorbeeld de TIPI, een vragenlijst met tien items die elk een van de Big Five-persoonlijkheidskenmerken meten: openness, consciensciousness, extraversion, agreeableness en neuroticism. De items hebben allemaal de stam 'Ik zie mezelf als...' en vervolgen dan met

-   extravert, enthousiast
-   kritisch, strijdzuchtig
-   betrouwbaar, gedisciplineerd
-   angstig, snel overstuur
-   open voor nieuwe ervaringen, complex
-   gereserveerd, stil
-   sympathiek, warm
-   slordig, achteloos
-   kalm, emotioneel stabiel
-   behoudend, niet creatief

Deelnemers reageren op een 7-puntsschaal die loopt van 'sterk mee oneens' tot 'sterk mee eens'.

Als hiervoor de variabelenamen `tipi1` tot en met `tipi10` worden gebruikt, dan begrijpen mensen die de TIPI kennen dat deze tien items persoonlijkheid meten. Een computer begrijpt ook dat deze tien items bij elkaar horen doordat de eerste vier karakters van elke variabelenaam hetzelfde zijn.

Deze variabelenamen kunnen nog worden verbeterd door in elke variabelenaam aan te geven wat er nu precies wordt gemeten. Dit kan door variabelenamen te gebruiken zoals `tipiExtraverted`, `tipiCritical`, `tipiDependable`, `tipiAnxious`, `tipiOpen`, `tipiReserved`, `tipiSympathetic`, `tipiDisorganized`, `tipiCalm`, `tipiConventional`. Dit is al een stuk beter te begrijpen voor mensen en is bovendien even leesbaar voor computers omdat de eerste vier karakters nog steeds hetzelfde zijn. In plaats van camelCase kan ook snake_case worden gebruikt (bijvoorbeeld `tipi_extraverted` en `tipi_disorganized`).

Een laatste verbetering is om ook het construct dat een item meet toe te voegen. Dan zouden de variabelenamen als volgt kunnen zijn: `tipiExtraExtraverted`, `tipiAgreeCritical`, `tipiConscDependable`, `tipiNeuroAnxious`, `tipiOpenOpen`, `tipiExtraReserved`, `tipiAgreeSympathetic`, `tipiConscDisorganized`, `tipiNeuroCalm`, `tipiOpenConventional` (of met snake_case: `tipi_extra_extraverted`, `tipi_consc_disorganized`, etc.).

Andere wetenschappers kunnen zo aan de kolomnamen al precies zien welke variabelen welke items representeren en bij welke constructen die items horen. Onder die andere wetenschappers kun je ook je toekomstige zelf rekenen. Vaak kom je namelijk na maanden of soms jaren weer terug bij een dataset en dan is het heel fijn als je de variabelenamen weer meteen begrijpt.

De nu gekozen variabelenamen zijn bovendien optimaal machine-readable. Doordat steeds dezelfde karakters worden gebruikt om aan te geven bij welk construct een item hoort, is het in veel software eenvoudig om uit alle variabelenamen in een dataset automatisch die variabelen op te zoeken die een bepaald construct meten, simpelweg door dat patroon te specificeren. Alle variabelenamen waarin `tipiExtra` voorkomt horen bijvoorbeeld bij het construct extraversie, en alle variabelenamen waarin `tipiConsc` voorkomt horen bij het construct consciëntieusheid.

Probeer bij het kiezen van variabelenamen dus altijd dit soort patronen in te bouwen. Op die manier is de dataset zo toegankelijk mogelijk voor mensen en machines en is de structuur van de dataset eenvoudig uit te leggen (bijvoorbeeld, alle variabelen die starten met `tipiExtra` meten extraversie).

De variabelenamen kies je overigens meestal als je je dataverzamelingssoftware configureert. Bij veel onderzoeken wordt bijvoorbeeld LimeSurvey gebruikt. De vraagcode die je daar specificeert voor een vraag wordt de variabelenaam in je dataset. Als een vraag subvragen heeft, plakt LimeSurvey de code die bij die subvraag hoort zelf aan de vraagcode met een laag liggend streepje. De (sub)vraagcodes zelf kunnen in LimeSurvey geen laag liggende streepjes bevatten, wat je dwingt tot het gebruik van camelCase.

Als in LimeSurvey dus de TIPI is ingevoerd in een vraag die als vraagcode `tipi` heeft, en als subvraagcodes `extraExtraverted` en `OpenConventional`, dan worden de variabelennamen in je dataset `tipi_extraExtraverted` en `tipi_OpenConventional`.

## Bestandsformaten en -namen {#datasets-bestandsformaten-en-namen}

Datasets kunnen worden opgeslagen in allerlei bestandsformaten. Sommige van deze bestandsformaten zijn open bestandsformaten: zij implementeren zogenaamde open standaarden, wat betekent dat ze in principe door iedereen geopend kunnen worden.

Andere bestandsformaten zijn 'proprietary', wat betekent dat ze in beheer zijn van een privaat bedrijf. Het openen van zo'n bestand vereist vaak de (betaalde) software van dat bedrijf. 

In lijn met het transparantieprincipe van wetenschappelijke integriteit (zie het hoofdstuk [Wetenschappelijke integriteit](https://openmens.nl/integriteit)) worden voor wetenschappelijke doeleinden proprietary-bestandsformaten zoveel mogelijk vermeden ten gunste van open bestandsformaten. Hieronder worden een aantal veelvoorkomende bestandsformaten voor datasets benoemd met hun extensie (de laatste karakters van de bestandsnaam, conventioneel gebruikt om het bestandstype aan te geven).

### Open bestandsformaten {#datasets-open-bestandformaten}

-   `.csv`: Comma Separated Values. Dit is een open bestandsformaat waarbij de data in platte tekst worden opgeslagen. Dit bestandsformaat heeft vaak de voorkeur omdat het zeer eenvoudig is. Het nadeel is dat het geen metadata ondersteunt.
-   `.ods`: OpenDocument Spreadsheet. Dit is een open bestandsformaat voor spreadsheets dat wordt beheerd door de OpenDocument Foundation.

### Deels open bestandsformaten {#datasets-deels-open-bestandsformaten}

-   `.xlsx`: Microsoft Excel Open XML Spreadsheet. Dit is een bestandsformaat dat formeel als open standaard is gepubliceerd, maar waarbij het bijbehorende programma, Excel, soms afwijkt van de standaard.

### Gesloten bestandsformaten {#datasets-gesloten-bestandsformaten}

-   `.sav`: SPSS Statistics Data File Format. Dit is een gesloten bestandsformaat dat wordt gebruikt door IBM SPSS Statistics. De specificaties ervan zijn niet openbaar gedocumenteerd, maar ontwikkelaars van opensource-software zijn erin geslaagd om voldoende over het bestandsformaat te leren om de ingesloten informatie te kunnen importeren en om ernaar te kunnen exporteren.

### Bestandsnamen {#datasets-bestandsnamen}

Het is belangrijk om bestandsnamen zo te kiezen dat ze zoveel mogelijk voor zichzelf spreken en op alle platforms goed te gebruiken zijn. Uit het eerste volgt dat je cryptische namen en afkortingen beter kunt vermijden. Uit het tweede volgen een aantal richtlijnen voor de karakters die je kunt gebruiken in bestandsnamen.

-   kleine letters (`a`, `b`, `c`, `d`, `e`, `f`, `g`, `h`, `i`, `j`, `k`, `l`, `m`, `n`, `o`, `p`, `q`, `r`, `s`, `t`, `u`, `v`, `w`, `x`, `y` & `z`)
-   hoofdletters (`A`, `B`, `C`, `D`, `E`, `F`, `G`, `H`, `I`, `J`, `K`, `L`, `M`, `N`, `O`, `P`, `Q`, `R`, `S`, `T`, `U`, `V`, `W`, `X`, `Y` & `Z`)
-   cijfers (`0`, `1`, `2`, `3`, `4`, `5`, `6`, `7`, `8` & `9`)
-   koppelteken (`-`)
-   laag liggend streepje (`_`)
-   punt (`.`)

Van deze lijst worden punten gebruikt om de extensie te scheiden van de rest van de bestandsnaam, dus het is verstandig om die te vermijden. Karakters die je niet moet gebruiken in bestandsnamen zijn bijvoorbeeld spaties (` `), komma's (`,`), apostroffen (`'`), uitroeptekens (`!`), haakjes (`(` en `)`) en alle andere tekens die niet in de lijst hierboven staan.

Net als variabelenamen moeten ook bestandsnamen Engelstalig zijn en zoveel mogelijk voor zichzelf spreken. Hierdoor zijn ze wereldwijd te begrijpen. Ideaal bezien is een verzameling bestanden voor een buitenstaander te begrijpen zonder weer een apart document waarin beschreven staat welke gegevens elk bestand bevat.

## Datasets openbaar maken {#datasets-datasets-openbaar-maken}

In de wetenschap is het tegenwoordig gebruikelijk om datasets openbaar te maken. Dit wordt in detail besproken in het hoofdstuk [Open data](https://openmens.nl/open-data). Bij het openbaar maken van data is het belangrijk om de principes in dit hoofdstuk te volgen. Daarmee worden data toegankelijker voor andere wetenschappers.

