---
output: html_document
editor_options: 
  chunk_output_type: console
---

# Discussie {#discussie}

```{r discussie-colofon, eval=FALSE, echo=FALSE, results="asis"}

md4e::knitWithClass(
  
  "Auteurs: Julia Fischmann en Natascha de Hoog; laatste update: 2023-10-20",

  class="colofon"
)

```

```{r discussie-summary, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="In dit hoofdstuk wordt besproken:",
  
  "Hoe schrijf je een discussie",
  "Resultaten en interpretatie",
  "Beperkingen en aanbevelingen",
  "Conclusie.",
 
  class="overview"
)

```

```{r discussie-courses, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="Deze stof wordt behandeld in de volgende Open Universiteitscursus(sen):",
  
  "Onderzoekspracticum experimenteel onderzoek (PB0422)",
  "Onderzoekspracticum bachelor thesis (PB9966)",
  
  "Onderzoekspracticum scriptieplan (PM9514)",
  
  class="courses"
)

```


## Inleiding

De discussie is het laatste deel van een wetenschappelijk artikel en is bedoeld om antwoord te geven op de vraagstelling, om de resultaten te verklaren en in een breder verband te plaatsen, en om de beperkingen van het onderzoek en de gevolgen ervan voor theorie en praktijk te bespreken. Het is belangrijk dat de discussie op zichzelf begrijpelijk is, zodat de lezer de hoofdpunten van het onderzoek snel en eenvoudig kan begrijpen, zonder het hele artikel te hoeven lezen.

De discussie schrijf je in de tegenwoordige tijd. Dit is in tegenstelling tot de resultatensectie die in verleden tijd wordt geschreven. Deze gaat namelijk over de resultaten die uit je onderzoek naar voren zijn gekomen ('In de steekproef was …'). Maar in de discussie ga je generaliseren naar de populatie en maak je uitspraken over het bestaan van bepaalde effecten ('In de populatie is …'). Enkel als je het hebt over de beperkingen van het onderzoek is de verleden tijd passend, omdat het hier weer gaat over het onderzoek dat je hebt uitgevoerd.

De discussie bestaat grofweg uit de volgende onderdelen:

- Samenvatting en interpretatie van de resultaten: je vat de bevindingen samen en legt deze uit in relatie tot de theorie

- Beperkingen en aanbevelingen: je bespreekt de nadelen van het onderzoek en je geeft suggesties voor vervolgonderzoek

- Slotconclusie: je bespreekt de implicaties van je onderzoek voor theorie en praktijk

Deze onderdelen geven een idee van hoe je de discussie kunt opbouwen, maar het zijn geen titels voor tussenkopjes. De discussie moet in de vorm van een verhaal geschreven worden, waarin de onderdelen soepel in elkaar overlopen. De drie basiscomponenten worden nu verder uitgelegd.


## Resultaten en interpretatie

Hieronder staan enkele punten die je moet behandelen in het eerste gedeelte van de discussie. Benader deze als aandachtspunten die je in het geheel gaat integreren. Als je de onderstaande punten namelijk als een checklijst benadert, loop je het risico dat je discussie gefragmenteerd en opsommend overkomt, terwijl het juist gaat om het integreren van de informatie.


### Herhaal de vraagstelling en geef een samenvatting van je resultaten

Begin elke discussie door de lezer te herinneren aan de doelstelling of vraagstelling van het onderzoek. Bijvoorbeeld door te schrijven 'Ons doel was om de verbanden tussen slaap en cognitieve prestaties van volwassenen te onderzoeken.'

Daarna geef je antwoord op de vraagstelling door een samenvatting te geven van je belangrijkste bevindingen. Bespreek hierbij de resultaten van je onderzoek, bijvoorbeeld 'Onze bevindingen suggereren dat er een positief verband bestaat tussen de hoeveelheid slaap en de cognitieve prestaties van volwassenen. Mensen die meer slapen, presteren beter op cognitieve testen dan mensen die minder slapen.'

Introduceer geen nieuwe bevindingen in de discussiesectie. Bespreek hoe je resultaten antwoord geven op je hypothese of deelvragen en voeg deze puzzelstukjes bij elkaar om zo een holistische conclusie te trekken over je centrale onderzoeksvraag. Bijvoorbeeld 'Onze bevindingen ondersteunen onze hypothese dat meer slaap tot betere cognitieve prestaties leidt.'


### Koppel de bevindingen aan de theorie

Je beschrijft hoe de bevindingen overeenkomen met of afwijken van bestaande theorieën en eerder onderzoek. Hierbij verbind je de bevindingen met de theorie die in de inleiding is besproken. Dit betekent dat je niet alleen kijkt naar de resultaten van je onderzoek, maar ook naar het veronderstelde mechanisme uit de theorie. Als de bevindingen overeenkomen met de theorie, bespreek je de meerwaarde van je eigen onderzoek. Als er afwijkingen zijn tussen je bevindingen en eerder onderzoek, probeer je deze te verklaren. Hierbij mag je ook nieuwe literatuur betrekken, want dit zijn uitkomsten die je op voorhand niet hebt voorzien.


### Valkuilen

Interpretatie en reflectie op de resultaten is belangrijk, maar hierbij kun je gemakkelijk in bepaalde valkuilen trappen. Let bij het schrijven van dit onderdeel op de onderstaande mogelijke valkuilen.

- Je herhaalt de resultaten of vat deze samen zonder de resultaten te interpreteren of te verbinden met de theorie en bevindingen van eerdere studies. Enkel een droge samenvatting volstaat niet. Het interpreteren en verbinden is essentieel.

- Je doet uitspraken over de steekproef in plaats van over de populatie. Het doel van een studie is om uitspraken te kunnen doen over patronen in de hele populatie en niet alleen in de getrokken steekproef. Houd dit in je achterhoofd bij het formuleren van uitspraken en vermeld eventuele beperkingen wat betreft de generaliseerbaarheid.

- Je geeft een technische samenvatting van de resultaten in plaats van een interpretatie. Statistische informatie hoort thuis in de resultatensectie en bespreek je niet opnieuw in de discussie.

- Je geeft een losse bespreking van deelvragen of hypotheses zonder deze resultaten te integreren. Let erop dat hoofdeffecten niet los gezien kunnen worden van interactie-effecten en probeer de resultaten in een groter verband te zien.

- Je vermeldt resultaten die niet eerder in de resultatensectie zijn beschreven. Alles wat met de data te maken heeft, moet eerst in de resultatensectie zijn beschreven voordat het in de discussie aan bod komt.

- Je trekt ongegronde of ongenuanceerde conclusies die niet voortvloeien uit de resultaten. Bijvoorbeeld, je doet stellige uitspraken over zwakke resultaten. Overweeg altijd de effectgrootte en vermijd stellige formuleringen. Bedenk dat een enkele studie slechts aanwijzingen kan geven voor het bestaan van een bepaald causaal mechanisme.


## Beperkingen en aanbevelingen

Hieronder vind je enkele cruciale overwegingen die je moet meenemen tijdens de beoordeling van je onderzoek. Het is belangrijk om deze niet als een afvinklijst te beschouwen, maar ze in het geheel te integreren. Hierdoor voorkom je dat je bespreking gefragmenteerd en opsommend overkomt.


### Bespreek alternatieve verklaringen

Bespreek alternatieve verklaringen voor de resultaten van je onderzoek. Het is belangrijk om te realiseren dat er meerdere interpretaties van de resultaten kunnen zijn en dat de theorie die het onderzoek lijkt te toetsen niet per se de enige juiste verklaring is. Door alternatieve verklaringen te bespreken, toon je aan dat je de resultaten van het onderzoek kritisch hebt bekeken en bewust bent van de grenzen ervan. Dit kan het vertrouwen in de bevindingen van het onderzoek vergroten. Probeer één overkoepelende alternatieve verklaring te formuleren voor afwijkende resultaten (zonder fragmentatie) en richt vooral de aandacht op verklaringen voor onverwachte resultaten. Voel je vrij om nieuwe bronnen aan te halen, aangezien je dit niet van tevoren had kunnen voorzien.

NB Dit punt gaat over theoretische verklaringen voor de resultaten, in tegenstelling tot het volgende punt waarbij het gaat om tekortkomingen in het onderzoeksdesign van jouw studie.


### Bespreek tekortkomingen

In de discussiesectie is het belangrijk om de beperkingen van het onderzoeksdesign te benoemen. Het gaat hierbij niet zozeer om fouten die zijn gemaakt, maar om het afbakenen van wat wel en wat niet geconcludeerd kan worden op basis van de uitkomsten van het onderzoek. Hierbij is het de bedoeling om duidelijk te maken in welke mate de resultaten beperkt zijn door de aard van het onderzoeksdesign. De beperkingen zijn dus niet een opsomming van ‘fouten’, maar dit onderdeel gaat om een afbakening van de grenzen van de conclusie: wat kan net wel, en wat kan net niet geconcludeerd worden op basis van dit onderzoek?

De beperkingen kunnen betrekking hebben op de generaliseerbaarheid van de resultaten naar een bredere populatie of op specifieke aspecten van de opzet van het onderzoek, zoals randomisatie, manipulatie van variabelen en de betrouwbaarheid van meetinstrumenten. Het is de taak van de onderzoeker om deze beperkingen uit te leggen en de mate waarin ze de resultaten of interpretatie ervan hebben beïnvloed te beargumenteren. Dit is niet een manier om het eigen onderzoek te misprijzen, maar eerder een evaluatie van de bevindingen binnen de context van de beperkingen van het onderzoek.

Als leidraad kun je de volgende aandachtspunten overwegen bij het evalueren van je onderzoek.

Generaliseerbaarheid (externe validiteit):

- Hoe representatief is de steekproef voor de populatie?

- Hoe specifiek zijn de omstandigheden waaronder het onderzoek is uitgevoerd?

Opzet van het onderzoek (interne validiteit):

- Zijn er andere variabelen die verschillen tussen de groepen en die de uitkomsten zouden kunnen verklaren?

- Is bij experimenteel onderzoek de manipulatie een goede vertaling van het causale mechanisme dat getest wordt?

- Waren er onvoorziene factoren die van invloed waren op de uitvoering van het onderzoek?

- Is er eventueel bias in de meetinstrumenten?


### Voorbeelden van het bespreken van beperkingen:

- Voorbeeld 1:
**Zwak beschreven**: 'Het onderzoek is alleen uitgevoerd bij volwassenen tussen de 25 en 35 jaar oud, waardoor de generaliseerbaarheid van de resultaten onbekend is voor andere leeftijdsgroepen.' **Sterker beschreven**: 'Een gerichte selectie op basis van leeftijd was nodig om de specifieke effecten van de interventie in deze groep te onderzoeken. Verder onderzoek is nodig om te bepalen of de resultaten ook op andere leeftijdsgroepen van toepassing zijn.'


- Voorbeeld 2: 
**Zwak beschreven**: 'De steekproefgrootte was beperkt, wat mogelijk geleid heeft tot een verminderde power en een verhoogd risico op type-II fouten.' **Sterker beschreven**: 'Het is aangetoond dat een N van minimaal 500 nodig is om voldoende power te hebben voor de verwachte effectgrootte in deze studie. Echter, door budgettaire beperkingen was het niet mogelijk om een grotere steekproefgrootte te realiseren. Hierdoor zijn er mogelijk effecten gemist die wel aanwezig waren.'

- Voorbeeld 3: 
**Zwak beschreven**: 'De selectie van de deelnemers was gericht op een specifieke groep met een bepaalde aandoening, wat mogelijk geleid heeft tot selectiebias.' **Sterker beschreven**: 'De selectie op deze specifieke aandoening was nodig om te onderzoeken of de interventie effect had op dit specifieke type klachten. Echter, door de gerichte selectie is het niet bekend of de resultaten ook van toepassing zijn bij mensen zonder deze aandoening. Verder onderzoek met een meer representatieve steekproef is nodig om de generaliseerbaarheid te bepalen.'


### Valkuilen

De interpretatie en reflectie op de beperkingen van een onderzoek en het doen van aanbevelingen is een complexe opgave waarbij studenten vaak tegen valkuilen aanlopen. Let bij het schrijven van dit onderdeel op de onderstaande mogelijke valkuilen en gebruik de gegeven suggesties voor het verbeteren van de discussie.

- Je denkt 'meer is beter'. Probeer niet een compleet lijstje met zo veel mogelijk beperkingen op te stellen. Het is beter om twee goed onderbouwde beperkingen te bespreken, dan tien losse bulletpoints kort aan te stippen.

- Je ziet beperkingen over het hoofd. Zorg ervoor dat je de belangrijkste beperkingen van je onderzoek beschrijft en hoe deze de resultaten kunnen hebben beïnvloed.

- Je onderbouwt de beperkingen niet. Beargumenteer en maak duidelijk hoe de beperkingen de resultaten van je onderzoek mogelijk hebben beïnvloed en hoe ze in de toekomst kunnen worden voorkomen.

- Je legt te veel nadruk op algemene beperkingen. Beperkingen die specifiek op je onderzoek van toepassing zijn, zijn veel sterker dan algemene beperkingen van onderzoek. Denk daarom goed na over beperkingen die specifiek op jouw onderzoek van toepassing zijn en werk deze uit en onderbouw ze.

- Je beschrijft irrelevante beperkingen. Beschrijf de belangrijkste beperkingen van je onderzoek, maar ga niet al te veel in op beperkingen die weinig impact hebben op de conclusies van je onderzoek. Dit kan de lezer afleiden van de waardevolle bevindingen van je onderzoek.

- Je geeft te beperkte aanbevelingen. Aanbevelingen moeten gericht zijn op het verkrijgen van aanvullende inzichten in het mechanisme en mogen niet beperkt blijven tot de beperkingen van je onderzoek.

- Je geeft geen onderbouwing voor de aanbevelingen voor verder onderzoek. Leg uit waarom vervolgonderzoek belangrijk is en welke aanvullende inzichten het kan opleveren.

- Je stelt aanbevelingen voor verder onderzoek gelijk aan de implicaties van je onderzoek. Onthoud dat aanbevelingen voor verder onderzoek en implicaties twee verschillende zaken zijn en bespreek deze apart.


## Conclusie: implicaties voor theorie en praktijk

De onderstaande aandachtspunten zijn bedoeld als richtlijn voor het schrijven van een sterk slotconclusie voor je scriptie. Zie deze punten niet als een afvinklijst, maar integreer ze in een samenhangend geheel. Benadering van deze punten als een checklijst kan leiden tot een gefragmenteerde en opsommende discussie, in plaats van een integratie van informatie.


### Trek een conclusie

Het is belangrijk om in het afsluitende deel van de discussie een bondige samenvatting te geven van de uitkomsten en bevindingen van je onderzoek in relatie tot de onderzoeksvraag en het doel. Hierbij gaat het niet om het opnieuw weergeven van de resultaten, maar om het trekken van een duidelijke conclusie op basis van deze resultaten. Deze conclusie moet in één of twee zinnen de hoofdbevindingen samenvatten en dient als overzicht van de meest relevante uitkomsten van je onderzoek.

Voorbeelden van conclusies:

- De resultaten van het onderzoek tonen aan dat er een significant verschil is tussen groep A en groep B wat betreft het gebruik van social media. Hieruit kunnen we concluderen dat groep A meer social media gebruikt dan groep B.

- Onze bevindingen suggereren dat er een verband bestaat tussen het niveau van onderwijs en inkomen. Hieruit kunnen we afleiden dat hoger onderwijs mogelijk leidt tot een hogere inkomensgroei.

- De analyse van de data heeft aangetoond dat er een toenemend aantal klachten is over de kwaliteit van product X. Op basis hiervan kunnen we stellen dat er actie nodig is om de kwaliteit van het product te verbeteren.


### Bespreek implicaties voor de praktijk

Om de resultaten van je onderzoek te verbinden aan de praktijk, is het belangrijk om de praktische implicaties te beschrijven en te verduidelijken. Het gaat hier om de bredere mogelijke toepassingen van de bevindingen. Als het veronderstelde mechanisme juist is, wat zijn dan de mogelijke toepassingsmogelijkheden in de praktijk? Je kunt hier ook aansluiten bij de maatschappelijke relevantie die je in je doelstelling hebt beschreven.

Voorbeelden van het bespreken van implicaties voor de praktijk:

- Onze resultaten suggereren dat interventies gericht op verbetering van assertiviteit bij jonge volwassenen significante verbeteringen kunnen laten zien in mentale gezondheid en emotionele regulatie. Een praktische implicatie hiervan is dat beleidsmakers en hulpverleners gericht kunnen werken aan het ontwikkelen van programma’s die de assertiviteitsvaardigheden van jonge volwassenen verbeteren.

- Onze studie toont aan dat ouders die regelmatig samen met hun kinderen sporten een positieve impact hebben op hun kinderen. Een praktische implicatie hiervan is dat scholen en sportverenigingen samen kunnen werken om programma’s te ontwikkelen die het samen sporten van ouders en kinderen bevorderen.

- Onze resultaten laten zien dat mindfulness-interventies effectief zijn voor het verbeteren van de emotieregulatie bij volwassenen met een angststoornis. Een praktische implicatie hiervan is dat hulpverleners en therapeuten mindfulness-interventies kunnen overwegen als onderdeel van hun behandelingen voor patiënten met angststoornissen.


### Bespreek implicaties voor de theorie

Bespreek hoe jouw resultaten van invloed zijn op de theorievorming en op het onderzoeksveld. Dit kan een uitdaging zijn, vooral als je niet bekend bent met de theorieën en literatuur die bij het onderzoek zijn gebruikt. Overweeg echter om een conclusie te trekken over het veronderstelde mechanisme. Hoe sterk is het bewijs dat dit bestaat? Moeten de theorieën worden aangepast op basis van jouw resultaten? Wat zijn de nieuwe (theoretische) vragen die jouw onderzoek oproept?

Voorbeelden van het bespreken van implicaties voor de theorie:

- Onze bevindingen suggereren dat het veronderstelde mechanisme tussen XYZ en ABC niet geheel juist is, wat betekent dat er verdere studies nodig zijn om dit mechanisme beter te begrijpen.

- Onze resultaten ondersteunen de bestaande theorie dat ABC de directe oorzaak is van XYZ en bieden nieuwe inzichten voor de verdere ontwikkeling van deze theorie.

- Onze resultaten zijn niet volledig in overeenstemming met de huidige theorieën en roepen nieuwe vragen op over de mogelijke verklaringen voor het verband tussen XYZ en ABC. Ook vragen ze om verder onderzoek.


### Valkuilen

Nadenken over de belangrijkste conclusie en implicaties van je bevindingen is een uitdagende taak. Je kunt hierbij met verschillende valkuilen te maken krijgen. Ben je dus bewust van onderstaande mogelijke valkuilen.

- Een afgeronde conclusie ontbreekt: Sommige discussies eindigen na het beschrijven van de beperkingen en/of aanbevelingen zonder een algemene conclusie. Zorg ervoor dat je discussie eindigt met een duidelijke algemene conclusie en een belangrijke implicatie voor het onderzoeksveld.

- Te lange of ingewikkelde conclusie: Soms is de conclusie te uitgebreid waardoor het niet duidelijk is wat de resultaten echt betekenen. Probeer dus op een bondige manier te beschrijven wat je nieuwe inzichten zijn uit het onderzoek.

- Stellige praktische implicaties: Het is belangrijk om ongenuanceerde stellingen te vermijden. Gebruik bijvoorbeeld geen 'moeten' bij het beschrijven van de praktische implicaties. Een enkel onderzoek is meestal niet voldoende om drastische veranderingen aan te bevelen.

- Te nauw ingestoken praktische implicaties: Soms richt de beschrijving van de praktische implicaties zich alleen op de specifieke manipulatie of interventie van het onderzoek en niet op het causale mechanisme wat de manipulatie moest blootleggen. Probeer de praktische implicaties dus breder te bezien.

- Ontbrekende theoretische implicaties: Soms wordt er helemaal geen poging gedaan om een uitspraak te doen over de bredere theorie of het onderzoeksveld. Zorg ervoor dat je ook de theoretische implicaties bespreekt en de bredere impact van je resultaten op het onderzoeksveld.
